package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import swing.PanelBorder;
import java.awt.Color;
import java.awt.SystemColor;
import component.Menu;
import dao.AccountDao;
import dao.StaffDao;
import event.EventMenuSelected;
import form.Account;
import form.Category;
import form.Form_Invoice;
import form.Home;
import form.Medicines;
import form.Provider;
import form.Sales;
import form.User;
import helper.ConvertString;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.border.LineBorder;
import swing.ImageAvatar;

public class JframeDashboard extends JFrame {

	private JPanel contentPane;
	private PanelBorder panelBorder;
	private Menu menu;
	private Home home;
	private User user;
	private Medicines medicines;
	private Provider provider;
	private Sales sales;
	private Form_Invoice inv;
	private Category cate;
	private Account account;
	private AccountDao accDao;
	private StaffDao staff;
	private ConvertString cvt;
	private String name = "fffff" ;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JframeDashboard frame = new JframeDashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JframeDashboard() {
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1132, 651);
		setBackground(new Color(0,0,0,0));
		home = new Home();
		user = new User();
		medicines = new Medicines();
		provider = new Provider();
		sales = new Sales();
		inv = new Form_Invoice();
		cate = new Category();
		account = new Account();
		accDao = new AccountDao();
		staff = new StaffDao();
		cvt = new ConvertString();
//		JOptionPane.showMessageDialog(this, getName());
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0,0,0,0));
		
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panelBorder = new PanelBorder();
		panelBorder.setBackground(Color.WHITE);
		panelBorder.setBounds(0, 0, 1132, 651);
		contentPane.add(panelBorder);
		panelBorder.setLayout(null);
		
		menu = new Menu();
		menu.setBounds(0, 0, 215, 651);
		panelBorder.add(menu);
		
		mainPanel = new JPanel();
		mainPanel.setOpaque(false);
		mainPanel.setBounds(216, 52, 916, 598);
		panelBorder.add(mainPanel);
		menu.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				menuMouseDragged(e);
			}
		});
		menu.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				menuMousePressed(e);
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				menuMouseClicked(e);
			}
		});
		menu.addEventMenuSelected(new EventMenuSelected() {
            public void selected(int index) {
            	if (JframeLogin.level == 1) {
            		if (index == 0) {
                        setForm(home);
                    } else if (index == 1) {
                        setForm(account);
                    } else if (index == 2) {
                        setForm(user);
                    } else if (index == 3) {
                        setForm(medicines);
                    } else if (index == 4) {
                        setForm(provider);
                    } else if (index == 5) {
                        setForm(sales);
                    } else if (index == 7) {
                        setForm(cate);
                    } else if (index == 6) {
                    	setForm(inv);
                    }else if (index == 8) {
                    	signOut();
                    } else if (index == 9) {
                    	exit();
                    }
            	} else if (JframeLogin.level == 2) {
            		if (index == 0) {
                        setForm(home);
                    } else if (index == 1) {
                        setForm(medicines);
                    } else if (index == 2) {
                        setForm(provider);
                    } else if (index == 3) {
                        setForm(cate);
                    } else if (index == 4) {
                        setForm(sales);
                    } else if (index == 5) {
                    	setForm(inv);
                    }else if (index == 6) {
                    	signOut();
                    } else if (index == 7) {
                    	exit();
                    }
            	} else {
            		 if (index == 0) {
                         setForm(inv);
                     } else if (index == 1) {
                     	 setForm(sales);
                     } else if (index == 2) {
                     	signOut();
                     } else if (index == 3) {
                     	exit();
                     } 
            	}
            }
        });
        //  set when system open start with home form
        if (JframeLogin.level == 1 || JframeLogin.level == 2) {
        	setForm(home);
        } else {
        	setForm(inv);
        }
        
        separator = new JSeparator();
        separator.setBounds(214, 52, 918, 2);
        panelBorder.add(separator);
        lblName = new JLabel();
        lblName.setBorder(null);
        lblName.setForeground(SystemColor.textHighlight);
        lblName.setFont(new Font("SansSerif", Font.BOLD, 13));
        lblName.setText(JframeLogin.username);
        lblName.setBounds(285, 7, 155, 22); 
        panelBorder.add(lblName);
        
        lblPosition = new JLabel((JframeLogin.level == 2) ? "Manager" : "Seller");
        lblPosition.setForeground(Color.GRAY);
        lblPosition.setFont(new Font("SansSerif", Font.BOLD, 12));
        lblPosition.setBounds(285, 26, 155, 16);
        panelBorder.add(lblPosition);
        
//        lblAvatar = new JLabel(new ImageIcon(
//				new ImageIcon("src/main/resources/Image/"+ staff.selectPictureByStaffId(staff.selectStaffByAccid(accDao.showAccId(JframeLogin.username))) +"")
//				.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH)));
//        lblAvatar.setBounds(225, 0, 50, 50);
//        panelBorder.add(lblAvatar);
        
        imageAvatar = new ImageAvatar();
        String url = staff.selectPictureByStaffId(staff.selectStaffByAccid(accDao.showAccId(JframeLogin.username)));
       
        if (url == null) {
        	url = "src/main/resources/img/noImg.png";
        	imageAvatar.setIcon(new ImageIcon(
    				new ImageIcon(url).getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH)));
        } else {
        	imageAvatar.setIcon(new ImageIcon(
     				new ImageIcon(url).getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH)));
        }
        
        imageAvatar.setBounds(225, 7, 40, 40);
        panelBorder.add(imageAvatar);
        
    }
	private void signOut() {
		if (JOptionPane.showConfirmDialog(this, "Are you sure want to sign out?","SIGN OUT",
				JOptionPane.YES_NO_OPTION) == 0) {
			JframeLogin login = new JframeLogin();
			login.setLocationRelativeTo(contentPane);
			login.setVisible(true);
			this.dispose();
			}
	}
	private void exit() {
		if (JOptionPane.showConfirmDialog(this, "Are you sure want to exit?","EXIT",
				JOptionPane.YES_NO_OPTION) == 0) {
			System.exit(0);
		}
	}
    private void setForm(JComponent com) {
        mainPanel.removeAll();
        mainPanel.add(com);
        mainPanel.repaint();
        mainPanel.revalidate();
    }
		
	private int xx;
    private int yy;
    private JPanel mainPanel;
    private JSeparator separator;
    private JLabel lblName;
    private JLabel lblPosition;
    private JLabel lblAvatar;
    private ImageAvatar imageAvatar;
   
	protected void menuMousePressed(MouseEvent e) {
		xx = e.getX();
		yy = e.getY();
	}
	protected void menuMouseDragged(MouseEvent e) {

        int x = e.getXOnScreen();

        int y = e.getYOnScreen();

        JframeDashboard.this.setLocation(x - xx, y - yy);  
	}
	protected void menuMouseClicked(MouseEvent e) {
		
	}
}
