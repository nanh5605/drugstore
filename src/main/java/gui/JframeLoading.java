package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.SplashScreen;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import extra.GradientPanel;

import javax.swing.JProgressBar;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;

public class JframeLoading extends JFrame {

	private JPanel contentPane;
	private JProgressBar loadingBar;
	private JLabel loadingLabel;
	private JLabel loadingValue;
	private JLabel lblLogo;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		JframeLoading frame = new JframeLoading();
		frame.setLocationRelativeTo(frame);
		frame.setVisible(true);
		try {
			for(int i = 1;i<=101;i++) {
				Thread.sleep(30);
				frame.loadingValue.setText(i + "%");
				
				if (i == 10) {
					frame.loadingLabel.setText("Turning on modules...");
				}
				if (i == 20) {
					frame.loadingLabel.setText("Loading modules...");
				}
				if (i == 50) {
					frame.loadingLabel.setText("Connecting to database...");
				}
				if (i == 70) {
					frame.loadingLabel.setText("Connection Susscessful !");
				}
				if (i == 80) {
					frame.loadingLabel.setText("Launching Application...");
				}
				if (i == 100) {
					JframeLogin login = new JframeLogin();
					frame.setVisible(false);
					login.setLocationRelativeTo(login);
					login.setVisible(true);
					
				}
				frame.loadingBar.setValue(i);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}

	/**
	 * Create the frame.
	 */
	public JframeLoading() {
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 634, 388);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GradientPanel gradientPanel = new GradientPanel(new Color(28, 181, 224), new Color(0, 0, 70), 0 );
		gradientPanel.setSize(new Dimension(800, 400));
		contentPane.add(gradientPanel);
		
		loadingBar = new JProgressBar();
		
		loadingLabel = new JLabel("Loading...");
		loadingLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		loadingLabel.setForeground(Color.WHITE);
		
		loadingValue = new JLabel("0 %");
		loadingValue.setHorizontalAlignment(SwingConstants.RIGHT);
		loadingValue.setForeground(Color.WHITE);
		loadingValue.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		lblLogo = new JLabel("");	
		ImageIcon dabIcon = new ImageIcon(JframeLoading.class.getResource("/icon/Medical-pharmacy-logo-design-template-on-transparent-background-PNG.png"));
		Image dabImage = dabIcon.getImage();
		Image modifiedDabImage = dabImage.getScaledInstance(200, 200, Image.SCALE_SMOOTH);
		dabIcon = new ImageIcon(modifiedDabImage);
		lblLogo.setIcon(dabIcon);
		
		lblNewLabel = new JLabel("ACT Phamacy");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 24));
		GroupLayout gl_gradientPanel = new GroupLayout(gradientPanel);
		gl_gradientPanel.setHorizontalGroup(
			gl_gradientPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_gradientPanel.createSequentialGroup()
					.addGroup(gl_gradientPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_gradientPanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_gradientPanel.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(loadingBar, GroupLayout.PREFERRED_SIZE, 608, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_gradientPanel.createSequentialGroup()
									.addComponent(loadingLabel, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(loadingValue, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))))
						.addGroup(gl_gradientPanel.createSequentialGroup()
							.addGap(193)
							.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_gradientPanel.createSequentialGroup()
							.addGap(223)
							.addComponent(lblNewLabel)))
					.addContainerGap(186, Short.MAX_VALUE))
		);
		gl_gradientPanel.setVerticalGroup(
			gl_gradientPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_gradientPanel.createSequentialGroup()
					.addGap(65)
					.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED, 236, Short.MAX_VALUE)
					.addGroup(gl_gradientPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(loadingLabel)
						.addComponent(loadingValue))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(loadingBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(23))
		);
		gradientPanel.setLayout(gl_gradientPanel);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGap(0, 624, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGap(0, 378, Short.MAX_VALUE)
		);
		contentPane.setLayout(gl_contentPane);
		
		
	}
}
