package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.RenderingHints;

import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Color;
import java.awt.Button;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import javax.swing.border.CompoundBorder;
import java.awt.Cursor;
import java.awt.Rectangle;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import dao.AccountDao;
import extra.GradientPanel;

public class JframeLogin extends JFrame {

	private JPanel contentPane;
	private JframeLoading load;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JframeLogin frame = new JframeLogin();
					frame.setUndecorated(true);
					frame.setLocationRelativeTo(frame);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JframeLogin() {
		setUndecorated(true);
		setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 653, 349);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		GradientPanel gradientLeft = new GradientPanel(new Color(28, 181, 224), new Color(0, 0, 70), 0 );
		gradientLeft.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				gradientPanelMousePressed(e);
			}
		});
		gradientLeft.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				gradientPanelMouseDragged(e);
			}
		});
		gradientLeft.setBounds(-8, 0, 661, 351);
		contentPane.add(gradientLeft);
		
		lblNewLabel_1 = new JLabel("ACT Phamacy");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 24));
		
		lblLogo = new JLabel("");
		ImageIcon dabIcon = new ImageIcon(JframeLoading.class.getResource("/icon/Medical-pharmacy-logo-design-template-on-transparent-background-PNG.png"));
		Image dabImage = dabIcon.getImage();
		Image modifiedDabImage = dabImage.getScaledInstance(200, 200, Image.SCALE_SMOOTH);
		dabIcon = new ImageIcon(modifiedDabImage);
		lblLogo.setIcon(dabIcon);
		
		txtuser = new JTextField();
		txtuser.setOpaque(false);
		txtuser.setBackground(new Color(255,255,255,0));
		txtuser.setCaretColor(SystemColor.text);
		txtuser.setFont(new Font("Arial", Font.PLAIN, 14));
		txtuser.setForeground(SystemColor.text);
		txtuser.setBorder(new MatteBorder(0, 0, 1, 0, (Color) SystemColor.text));
		txtuser.setColumns(10);
		
		lblNewLabel_2 = new JLabel("User: ");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblPassword = new JLabel("Password: ");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblClose = new JLabel("X");
		lblClose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblCloseMouseClicked(e);
			}
		});
		lblClose.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblClose.setHorizontalAlignment(SwingConstants.CENTER);
		lblClose.setForeground(Color.WHITE);
		lblClose.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblClose.setBackground(Color.BLUE);
		
		txtpassword = new JPasswordField();
		txtpassword.setOpaque(false);
		txtpassword.setBackground(new Color(255,255,255,0));
		txtpassword.setCaretColor(SystemColor.text);
		txtpassword.setForeground(SystemColor.text);
		txtpassword.setFont(new Font("Arial", Font.PLAIN, 14));
		txtpassword.setBorder(new MatteBorder(0, 0, 1, 0, (Color) SystemColor.text));
		
		lblSignIn = new JLabel("Sign In");
		lblSignIn.setHorizontalAlignment(SwingConstants.CENTER);
		lblSignIn.setForeground(Color.WHITE);
		lblSignIn.setFont(new Font("Arial", Font.BOLD, 30));
		
		panel = new JPanel();
		panel.setToolTipText("");
		panel.setBorder(new LineBorder(Color.WHITE, 1, true));
		panel.setBackground(new Color(0, 0, 70, 10));
		GroupLayout gl_gradientLeft = new GroupLayout(gradientLeft);
		gl_gradientLeft.setHorizontalGroup(
			gl_gradientLeft.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_gradientLeft.createSequentialGroup()
					.addGroup(gl_gradientLeft.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_gradientLeft.createSequentialGroup()
							.addGap(69)
							.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_gradientLeft.createSequentialGroup()
							.addGap(51)
							.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)))
					.addGroup(gl_gradientLeft.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_gradientLeft.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, 112, Short.MAX_VALUE)
							.addGroup(gl_gradientLeft.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_gradientLeft.createSequentialGroup()
									.addGap(230)
									.addComponent(lblClose, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblSignIn, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_gradientLeft.createSequentialGroup()
									.addGap(1)
									.addComponent(lblNewLabel_2))
								.addComponent(txtuser, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_gradientLeft.createSequentialGroup()
									.addGap(1)
									.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
								.addComponent(txtpassword, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE))
							.addContainerGap())
						.addGroup(gl_gradientLeft.createSequentialGroup()
							.addGap(182)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		gl_gradientLeft.setVerticalGroup(
			gl_gradientLeft.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_gradientLeft.createSequentialGroup()
					.addComponent(lblLogo, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					.addGap(62))
				.addGroup(gl_gradientLeft.createSequentialGroup()
					.addGroup(gl_gradientLeft.createParallelGroup(Alignment.LEADING)
						.addComponent(lblClose, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_gradientLeft.createSequentialGroup()
							.addGap(25)
							.addComponent(lblSignIn, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)))
					.addGap(14)
					.addComponent(lblNewLabel_2)
					.addGap(11)
					.addComponent(txtuser, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
					.addGap(17)
					.addComponent(lblPassword)
					.addGap(14)
					.addComponent(txtpassword, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(38)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addGap(33))
		);
		
		lblNewLabel = new JLabel("SIGN IN");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblNewLabelMouseClicked(e);
			}
		});
		lblNewLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 15));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addComponent(lblNewLabel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addComponent(lblNewLabel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
		);
		panel.setLayout(gl_panel);
		gradientLeft.setLayout(gl_gradientLeft);
		
		GradientPanel gradientButton = new GradientPanel(new Color(1,217,105), new Color(0,112,143), 3 );
	}
	int xx,xy;
	private JLabel lblNewLabel_1;
	private JLabel lblLogo;
	private JLabel lblNewLabel;
	private JTextField txtuser;
	private JLabel lblNewLabel_2;
	private JLabel lblPassword;
	private JLabel lblClose;
	private JPasswordField txtpassword;
	private JLabel lblSignIn;
	private JPanel panel;
	protected void gradientPanelMouseDragged(MouseEvent e) {
		 
	        int x = e.getXOnScreen();

	        int y = e.getYOnScreen();

	        JframeLogin.this.setLocation(x - xx, y - xy);  
	}
	protected void gradientPanelMousePressed(MouseEvent e) {
		 xx = e.getX();

	        xy = e.getY();
	}
	protected void lblCloseMouseClicked(MouseEvent e) {
		if (JOptionPane.showConfirmDialog(this, "Are you sure want to exit?","EXIT",
				JOptionPane.YES_NO_OPTION) == 0) {
			System.exit(0);
		}
	}
	public static String username;
//	public static int permission;
	public static int level;
	protected void lblNewLabelMouseClicked(MouseEvent e) {
		AccountDao dao = new AccountDao();
		if (txtuser.getText().equals("Act-admin") && txtpassword.getText().equals("0208"))
		{
			username = txtuser.getText();
			level = 1;
			this.setVisible(false);
			JframeDashboard jDashboard = new JframeDashboard();
			jDashboard.setLocationRelativeTo(jDashboard);
			jDashboard.setVisible(true);
			
		}
		else if(dao.login(txtuser.getText(), txtpassword.getText()) > 0) {
			username = txtuser.getText();
			if(dao.level(txtuser.getText(), txtpassword.getText()) == 1 || dao.level(txtuser.getText(), txtpassword.getText()) == 2) {
//				this.setVisible(false);
//				JframeDashboard jDashboard = new JframeDashboard();
//				jDashboard.setLocationRelativeTo(jDashboard);
//				jDashboard.setVisible(true);
//				permission = 1;
				level = dao.level(txtuser.getText(), txtpassword.getText());
			}else {
//				this.setVisible(false);
//				JframeDashboardforUser jDashboardUser = new JframeDashboardforUser();
//				jDashboardUser.setLocationRelativeTo(jDashboardUser);
//				jDashboardUser.setVisible(true);
//				permission = 0;
				level = dao.level(txtuser.getText(), txtpassword.getText());
			}
			this.setVisible(false);
			JframeDashboard jDashboard = new JframeDashboard();
			jDashboard.setLocationRelativeTo(jDashboard);
			jDashboard.setVisible(true);
		}
		else if(txtuser.getText().equals("Act-user") && txtpassword.getText().equals("1997")) {
			username = txtuser.getText();
			level = 0;
			this.setVisible(false);
			JframeDashboard jDashboard = new JframeDashboard();
			jDashboard.setLocationRelativeTo(jDashboard);
			jDashboard.setVisible(true);
		}
		else {
			JOptionPane.showMessageDialog(this, "Email or Password is not correct");
		}
	}
}
