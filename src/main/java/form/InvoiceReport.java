package form;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.SaleDao;
import helper.FormatNumber;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

public class InvoiceReport extends JFrame {

	private JPanel contentPane;
//	private double price;
//	private int quantity;
//	private double total;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InvoiceReport frame = new InvoiceReport();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InvoiceReport() {
		setTitle("InvoiceReport");
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 643, 630);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		showRP();
	}
	
	public void showRP() {
		try {
			List<Map<String, ?>> list = new ArrayList<Map<String,?>>();
			FormatNumber fn = new FormatNumber();
			SaleDao sale = new SaleDao();
			Form_Invoice inv = new Form_Invoice();
			for(var in : sale.getListInvoiceDetail(Form_Invoice.invoiceID)) {
				Map<String, Object> map = new HashMap<String,Object>();
				map.put("name", in.getProName());
				map.put("type", in.getUnit());
				map.put("quantity", in.getQuantity());
				map.put("price", in.getPrice());
				map.put("totalProduct", in.getQuantity()*in.getPrice());
				map.put("InvoiceID", Form_Invoice.invoiceID);
				list.add(map);
			}
			JasperPrint print = JasperFillManager.fillReport(JasperCompileManager.compileReport("Report/Invoice.jrxml"),
			null, new JRBeanCollectionDataSource(list));
			this.getContentPane().add(new JRViewer(print));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}

}
