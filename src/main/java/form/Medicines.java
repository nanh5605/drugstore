package form;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.DefaultRowSorter;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
//import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowFilter;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import com.toedter.calendar.JDateChooser;

import extra.Table;
import helper.RegexConst;
import helper.Validation;
import component.MyButton;
import dao.CategoryDao;
import dao.MedicineDao;
import dao.ProviderDao;
import entity.Categories;
import entity.Medicine;
//import model.StatusType;

import java.awt.SystemColor;
import java.awt.Cursor;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
//import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Currency;
import java.util.Locale;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Insets;

public class Medicines extends JPanel {
	private JLabel lblIdPro;
	private JLabel lblProName;
	private JLabel lblPrice;
	private JLabel lblQuantity;
	private JLabel lblCategory;
	private JLabel lblIngridents;
	private JLabel lblUses;
	private JLabel lblType;
	private JLabel lblBrand;
	private JLabel lblMfDate;
	private JLabel lblExpireDate;
	private JLabel lblProvider;
	private JLabel lblPicture;
	private JTextField txtId;
	private JTextField txtPrice;
	private JTextField txtQuantity;
	private JTextField txtUse;
	private JTextField txtType;
	private JLabel lblDecristion;
	private JTextPane txtDecription;
	private JTextField txtName;
	private JTextField TxtIngridents;
	private JTextField txtBrand;
	private JDateChooser dcMfDate;
	private JDateChooser dcExDate;
	private MyButton mbtnClear;
	private MyButton mbtnDelete;
	private MyButton mbtnUpdate;
	private MyButton mbtnAdd;
	private JComboBox cbProvider;
	private JScrollPane scrollPane;
	private Table table;
	private JLabel lblNewLabel;
	private JTextField txtSearch;
	private JComboBox cbCategory;
	private MyButton btnNewButton;
	private JLabel lblIconPicture;
	private JTextField txtPicture;
	private MyButton mbtnCopy;

	/**
	 * Create the panel.
	 */
	public Medicines() {
		setBackground(Color.WHITE);
		setBounds(0,0,916,588);
		
		lblIdPro = new JLabel("ID");
		lblIdPro.setSize(23, 26);
		lblIdPro.setLocation(15, 44);
		lblIdPro.setFont(new Font("Arial", Font.BOLD, 12));
		lblIdPro.setForeground(SystemColor.textHighlight);
		
		lblProName = new JLabel("Name");
		lblProName.setFont(new Font("Arial", Font.BOLD, 12));
		lblProName.setForeground(SystemColor.textHighlight);
		
		lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Arial", Font.BOLD, 12));
		lblPrice.setForeground(SystemColor.textHighlight);
		
		lblQuantity = new JLabel("Quantity");
		lblQuantity.setFont(new Font("Arial", Font.BOLD, 12));
		lblQuantity.setForeground(SystemColor.textHighlight);
		
		lblCategory = new JLabel("Category");
		lblCategory.setLocation(403, 37);
		lblCategory.setFont(new Font("Arial", Font.BOLD, 12));
		lblCategory.setForeground(SystemColor.textHighlight);
		
		lblIngridents = new JLabel("Ingridents");
		lblIngridents.setFont(new Font("Arial", Font.BOLD, 12));
		lblIngridents.setForeground(SystemColor.textHighlight);
		
		lblUses = new JLabel("Uses");
		lblUses.setFont(new Font("Arial", Font.BOLD, 12));
		lblUses.setForeground(SystemColor.textHighlight);
		
		lblType = new JLabel("Type");
		lblType.setFont(new Font("Arial", Font.BOLD, 12));
		lblType.setForeground(SystemColor.textHighlight);
		
		lblBrand = new JLabel("Brand");
		lblBrand.setFont(new Font("Arial", Font.BOLD, 12));
		lblBrand.setForeground(SystemColor.textHighlight);
		
		lblMfDate = new JLabel("MFDate");
		lblMfDate.setFont(new Font("Arial", Font.BOLD, 12));
		lblMfDate.setForeground(SystemColor.textHighlight);
		
		lblExpireDate = new JLabel("EXDate");
		lblExpireDate.setFont(new Font("Arial", Font.BOLD, 12));
		lblExpireDate.setForeground(SystemColor.textHighlight);
		
		lblProvider = new JLabel("Provider");
		lblProvider.setFont(new Font("Arial", Font.BOLD, 12));
		lblProvider.setForeground(SystemColor.textHighlight);
		
		lblPicture = new JLabel("Picture");
		lblPicture.setFont(new Font("Arial", Font.BOLD, 12));
		lblPicture.setForeground(SystemColor.textHighlight);
		
		txtId = new JTextField();
		txtId.setEditable(false);
		txtId.setLocation(102, 45);
		txtId.setBorder(new MatteBorder(0, 0, 1, 0, (Color) Color.BLACK));
		txtId.setOpaque(false);
		txtId.setColumns(10);
		
		txtPrice = new JTextField();
		txtPrice.setOpaque(false);
		txtPrice.setColumns(10);
		txtPrice.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		txtQuantity = new JTextField();
		txtQuantity.setOpaque(false);
		txtQuantity.setColumns(10);
		txtQuantity.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		txtUse = new JTextField();
		txtUse.setOpaque(false);
		txtUse.setColumns(10);
		txtUse.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		txtType = new JTextField();
		txtType.setOpaque(false);
		txtType.setColumns(10);
		txtType.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		setLayout(null);
		
		lblDecristion = new JLabel("Description");
		lblDecristion.setFont(new Font("Arial", Font.BOLD, 12));
		lblDecristion.setForeground(SystemColor.textHighlight);
		
		txtDecription = new JTextPane();
		txtDecription.setMargin(new Insets(3, 3, 3, 0));
		txtDecription.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		txtDecription.setAutoscrolls(true);

		txtName = new JTextField();
		txtName.setOpaque(false);
		txtName.setColumns(10);
		txtName.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		TxtIngridents = new JTextField();
		TxtIngridents.setOpaque(false);
		TxtIngridents.setColumns(10);
		TxtIngridents.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		txtBrand = new JTextField();
		txtBrand.setOpaque(false);
		txtBrand.setColumns(10);
		txtBrand.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		dcMfDate = new JDateChooser();
		dcMfDate.setBounds(460, 76, 114, 30);
		
		dcExDate = new JDateChooser();
		dcExDate.setBounds(460, 109, 114, 35);
		
		mbtnClear = new MyButton();
		mbtnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnClear_actionPerformed(e);
			}
		});
		mbtnClear.setColorOver(new Color(0, 191, 255));
		mbtnClear.setColorClick(new Color(100, 149, 237));
		mbtnClear.setBorderColor(SystemColor.textHighlight);
		mbtnClear.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		mbtnClear.setBorder(null);
		mbtnClear.setText("Clear");
		mbtnClear.setRadius(15);
		
		mbtnDelete = new MyButton();
		mbtnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnDelete_actionPerformed(e);
			}
		});
		mbtnDelete.setColorOver(new Color(0, 191, 255));
		mbtnDelete.setColorClick(new Color(100, 149, 237));
		mbtnDelete.setBorderColor(SystemColor.textHighlight);
		mbtnDelete.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		mbtnDelete.setText("Delete");
		mbtnDelete.setRadius(15);
		mbtnDelete.setBorder(null);
		mbtnDelete.setEnabled(false);
		
		mbtnUpdate = new MyButton();
		mbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnUpdate_actionPerformed(e);
			}
		});
		mbtnUpdate.setColorOver(new Color(0, 191, 255));
		mbtnUpdate.setColorClick(new Color(100, 149, 237));
		mbtnUpdate.setBorderColor(SystemColor.textHighlight);
		mbtnUpdate.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		mbtnUpdate.setText("Update");
		mbtnUpdate.setRadius(15);
		mbtnUpdate.setBorder(null);
		mbtnUpdate.setEnabled(false);
		
		mbtnAdd = new MyButton();
		mbtnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnAdd_actionPerformed(e);
			}
		});
		mbtnAdd.setColorOver(new Color(0, 191, 255));
		mbtnAdd.setColorClick(new Color(100, 149, 237));
		mbtnAdd.setBorderColor(SystemColor.textHighlight);
		mbtnAdd.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		mbtnAdd.setText("Add");
		mbtnAdd.setRadius(15);
		mbtnAdd.setBorder(null);
		
		cbProvider = new JComboBox();
		
		cbCategory = new JComboBox();
		
		
		scrollPane = new JScrollPane();
		loadCombobox();
		
		table = new Table();
		MedicineDao dao = new MedicineDao();
		loadMedicine(dao);
	
		table.setAutoCreateRowSorter(true);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tableMouseClicked(e);
			}
		});
		scrollPane.setViewportView(table);
		
		lblNewLabel = new JLabel("Search:");
		lblNewLabel.setForeground(SystemColor.textHighlight);
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 13));
		
		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				do_txtSearch_keyPressed(e);
			}
		});
		txtSearch.setOpaque(false);
		txtSearch.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtSearch.setColumns(10);
		
		btnNewButton = new MyButton();
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_btnNewButton_actionPerformed(e);
			}
		});
		btnNewButton.setColorOver(new Color(0, 191, 255));
		btnNewButton.setColorClick(new Color(100, 149, 237));
		btnNewButton.setBorderColor(SystemColor.textHighlight);
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.setText("Import");
		btnNewButton.setRadius(15);
		btnNewButton.setBorder(null);
		
		txtPicture = new JTextField();
		txtPicture.setOpaque(false);
		txtPicture.setColumns(10);
		txtPicture.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtPicture.setVisible(false);
//		txtPicture.setOpaque(false);
//		txtPicture.setColumns(10);
		lblIconPicture = new JLabel("");
		
		mbtnCopy = new MyButton();
		mbtnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnCopy_actionPerformed(e);
			}
		});
		mbtnCopy.setText("Copy");
		mbtnCopy.setRadius(15);
		mbtnCopy.setColorOver(new Color(0, 191, 255));
		mbtnCopy.setColorClick(new Color(100, 149, 237));
		mbtnCopy.setBorderColor(SystemColor.textHighlight);
		mbtnCopy.setBorder(null);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblIdPro, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblProName, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(txtId, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtName, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblBrand, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblIngridents, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
					.addGap(1)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(txtBrand, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(TxtIngridents, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(5)
							.addComponent(lblCategory, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
						.addComponent(lblProvider, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(cbCategory, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(cbProvider, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
					.addGap(27)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblPicture, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
						.addComponent(txtPicture, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
					.addGap(43)
					.addComponent(lblIconPicture, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblPrice, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuantity, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(16)
							.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(txtPrice, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtQuantity, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(30)
							.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblUses, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
									.addGap(33)
									.addComponent(txtUse, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(lblMfDate, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblType, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
									.addGap(33)
									.addComponent(txtType, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(lblExpireDate, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)))
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(dcMfDate, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
								.addComponent(dcExDate, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(lblDecristion, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addGap(35)
							.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addGap(34)
							.addComponent(mbtnCopy, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
					.addGap(4)
					.addComponent(txtDecription, GroupLayout.PREFERRED_SIZE, 208, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 286, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(15)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 883, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(23)
							.addComponent(lblIdPro, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(21)
							.addComponent(lblProName))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(23)
							.addComponent(txtId, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(txtName, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(22)
							.addComponent(lblBrand)
							.addGap(26)
							.addComponent(lblIngridents))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(14)
							.addComponent(txtBrand, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(27)
							.addComponent(TxtIngridents, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(lblCategory)
							.addGap(28)
							.addComponent(lblProvider))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(16)
							.addComponent(cbCategory, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
							.addGap(17)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(cbProvider, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtPicture, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(6)
									.addComponent(lblPicture))
								.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
						.addComponent(lblIconPicture, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(lblPrice)
							.addGap(19)
							.addComponent(lblQuantity)
							.addGap(36)
							.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(2)
							.addComponent(txtPrice, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(txtQuantity, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(36)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnCopy, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(35)
							.addComponent(lblDecristion))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(8)
							.addComponent(txtDecription, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(6)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(lblUses)
									.addComponent(txtUse, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblMfDate))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(lblType)
									.addComponent(txtType, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblExpireDate)))
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(dcMfDate, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(dcExDate, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
					.addGap(1)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(8)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE))
		);
		setLayout(groupLayout);

	}
	public void loadCombobox() {
		ProviderDao proD = new ProviderDao();
		proD.getListProvider().forEach(x->cbProvider.addItem(x.getName()));
		CategoryDao cateD = new CategoryDao();
		cateD.getListCategory().forEach(x-> {if(x.isStatus() == true) {cbCategory.addItem(x.getName());}} );
	}
	
	protected void do_mbtnAdd_actionPerformed(ActionEvent e) {
		MedicineDao dao = new MedicineDao();
		Medicine med = new Medicine();
		if(validform() == false) {
			return;
		}
		CategoryDao cateD = new CategoryDao();
		ProviderDao proD = new ProviderDao();
		
		med.setName(txtName.getText());
		
		cateD.getListCategory().forEach(y-> {if(y.getName().equals( cbCategory.getSelectedItem()))
			med.setCateID(y.getCateID());
		});
		med.setDecription(txtDecription.getText());
		med.setPrice(Float.valueOf(txtPrice.getText()));
		proD.getListProvider().forEach(y-> {if(y.getName().equals(cbProvider.getSelectedItem())) 
			med.setProvider(y.getProviderID());
		});
		
		
		med.setDateOfManufacture(	
			LocalDate.ofInstant(dcMfDate.getDate().toInstant(),
					ZoneId.systemDefault())
		);
		med.setExpiryDate(	
			LocalDate.ofInstant(dcExDate.getDate().toInstant(),
					ZoneId.systemDefault())
		);
		if(med.getDateOfManufacture().isAfter(med.getExpiryDate())){
			JOptionPane.showMessageDialog(this, "MFDate has been before EXDate");
			return;
        }
		if(med.getDateOfManufacture().isAfter(java.time.LocalDate.now())){
			JOptionPane.showMessageDialog(this, "MFDate is invalid!");
			return;
        }
		if(med.getDateOfManufacture().isAfter(med.getExpiryDate().minusMonths(2))){
			JOptionPane.showMessageDialog(this, "EXDate is invalid!");
			return;
        }
		try {
			med.setCreateDate( LocalDate.now());
			
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		med.setPicture(txtPicture.getText());
		med.setIngredients(TxtIngridents.getText());
		med.setUses(txtUse.getText());
		med.setBrand(txtBrand.getText());
		med.setType(txtType.getText());
		med.setQuantity(Integer.parseInt(txtQuantity.getText()));
		try {
			med.setCreateDate(LocalDate.now());
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		dao.insertMedicine(med);
		
		JOptionPane.showMessageDialog(this, "Add successfully!");
		clearText();
		loadMedicine(dao);	
		Home.lblValue.setText(String.valueOf(dao.sumProduct()));
	}
	protected void tableMouseClicked (MouseEvent e) {
		mbtnUpdate.setEnabled(true);
		mbtnDelete.setEnabled(true);
		mbtnCopy.setEnabled(true);
		mbtnAdd.setEnabled(false);
		int rowIndex = table.getSelectedRow();
		MedicineDao dao = new MedicineDao();
		CategoryDao cateD = new CategoryDao();
		ProviderDao proD = new ProviderDao();
		
		txtId.setText(table.getValueAt(rowIndex, 0).toString());
		
		var id = table.getModel().getValueAt(table.getSelectedRow(),0).toString();
		
		dao.getListMedicine().forEach(x-> {if(x.getProID() == Integer.parseInt(id)) {
			txtId.setText(String.valueOf(x.getProID()));
			txtName.setText(x.getName());
			txtPrice.setText(String.valueOf(formatMoney(x.getPrice())));
			txtUse.setText(x.getUses());
			txtBrand.setText(x.getBrand());
			txtDecription.setText(x.getDecription());
			txtType.setText(x.getType());
			TxtIngridents.setText(x.getIngredients());
			txtBrand.setText(x.getBrand());
			txtQuantity.setText(String.valueOf(x.getQuantity()));
			cateD.getListCategory().forEach(y-> {if(y.getCateID() == x.getCateID())
				cbCategory.setSelectedItem(y.getName());
			});
			proD.getListProvider().forEach(y-> {if(y.getProviderID() == x.getProvider())
				cbProvider.setSelectedItem(y.getName());
			});
			try {
				dcMfDate.setDate(
					new SimpleDateFormat("yyyy-MM-dd")
						.parse(x.getDateOfManufacture().toString())
				);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
			try {
				dcExDate.setDate(
					new SimpleDateFormat("yyyy-MM-dd")
						.parse(x.getExpiryDate().toString())
				);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			txtPicture.setText(x.getPicture());
			lblIconPicture.setIcon(new ImageIcon(
									new ImageIcon(txtPicture.getText()).getImage().getScaledInstance(100, 80, Image.SCALE_SMOOTH)));			
			
		}});
		
	}
	protected void do_mbtnClear_actionPerformed(ActionEvent e) {
		if(cbCategory.getItemCount() != 0){
			cbCategory.removeAllItems(); 
		}
		if(cbProvider.getItemCount() != 0){
			cbProvider.removeAllItems(); 
		}
		loadCombobox();
		clearText();
	}
	
	protected void do_mbtnDelete_actionPerformed(ActionEvent e) {
		
		MedicineDao dao = new MedicineDao();
		if(txtId.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Nothing to delete");
			return;
		} 
		if(JOptionPane.showConfirmDialog(this, "Are you sure want to delete?", "DELETE",
				JOptionPane.YES_NO_OPTION) == 0) {
			int rowIndex = table.getSelectedRow();
			txtId.setText(table.getValueAt(rowIndex, 0).toString());
			int id = (int) table.getModel().getValueAt(table.getSelectedRow(),0);
			dao.deleteMedicine(id);
			JOptionPane.showMessageDialog(this, "Delete successfully");
			loadMedicine(dao);
			Home.lblValue.setText(String.valueOf(dao.sumProduct()));
			clearText();
		}
		
	}
	protected void do_mbtnUpdate_actionPerformed(ActionEvent e) {
		MedicineDao dao = new MedicineDao();
		Medicine med = new Medicine();
		CategoryDao cateD = new CategoryDao();
		ProviderDao proD = new ProviderDao();
		
		if(txtId.getText().equals("")){
			JOptionPane.showMessageDialog(this, "Please choose medicine you want to update!");
			return;
		}
		if(validform() == false) {
			return;
		}
		
		med.setName(txtName.getText());
		cateD.getListCategory().forEach(y-> {if(y.getName().equals( cbCategory.getSelectedItem()))
			med.setCateID(y.getCateID());
		});
		med.setDecription(txtDecription.getText());
		med.setPrice(Float.valueOf(txtPrice.getText()));
		proD.getListProvider().forEach(y-> {if(y.getName().equals(cbProvider.getSelectedItem())) 
			med.setProvider(y.getProviderID());
		});
		med.setDateOfManufacture(	
			LocalDate.ofInstant(dcMfDate.getDate().toInstant(),
					ZoneId.systemDefault())
		);
		med.setExpiryDate(	
			LocalDate.ofInstant(dcExDate.getDate().toInstant(),
					ZoneId.systemDefault())
		);
		if(med.getDateOfManufacture().isAfter(med.getExpiryDate())){
			JOptionPane.showMessageDialog(this, "MFDate has been before EXDate");
			return;
        }
		if(med.getDateOfManufacture().isAfter(java.time.LocalDate.now())){
			JOptionPane.showMessageDialog(this, "MFDate is invalid!");
			return;
        }
		if(med.getDateOfManufacture().isAfter(med.getExpiryDate().minusMonths(2))){
			JOptionPane.showMessageDialog(this, "EXDate is invalid!");
			return;
        }
		med.setPicture(txtPicture.getText());
		med.setIngredients(TxtIngridents.getText());
		med.setUses(txtUse.getText());
		med.setBrand(txtBrand.getText());
		med.setType(txtType.getText());
		med.setQuantity(Integer.parseInt(txtQuantity.getText()));
		
		med.setProID(Integer.parseInt(txtId.getText()));
		
		dao.updateMedicine(med);
		JOptionPane.showMessageDialog(this, "Update successfully!");

		loadMedicine(dao);	
		Home.lblValue.setText(String.valueOf(dao.sumProduct()));
		clearText();
	}
	private void clearText() {
		MedicineDao dao = new MedicineDao();
		loadMedicine(dao);
		mbtnAdd.setEnabled(true);
		mbtnUpdate.setEnabled(false);
		mbtnDelete.setEnabled(false);
		mbtnCopy.setEnabled(false);
		
		
		txtId.setText("");
		txtName.setText("");
		txtPrice.setText("");
		txtUse.setText("");
		txtBrand.setText("");
		txtDecription.setText("");
		txtType.setText("");
		TxtIngridents.setText("");
		txtBrand.setText("");
		txtQuantity.setText("");
		cbCategory.setSelectedItem("");
		cbProvider.setSelectedItem("");
		try {
			dcMfDate.setDate(
				new SimpleDateFormat("yyyy-MM-dd")
					.parse(java.time.LocalDate.now().toString())
			);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		try {
			dcExDate.setDate(
				new SimpleDateFormat("yyyy-MM-dd")
					.parse(java.time.LocalDate.now().toString())
			);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		lblIconPicture.setIcon(
				new ImageIcon(
				new ImageIcon("src/main/resources/img/noImg.png")
					.getImage().getScaledInstance(100, 80, Image.SCALE_SMOOTH)));
	}
	private void loadMedicine (MedicineDao dao) {
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("Id");
		model.addColumn("#");
		model.addColumn("Name");
		model.addColumn("Price");
		model.addColumn("Quantity");
		model.addColumn("Type");
		model.addColumn("Create date");
		
		for(int i = 0; i < dao.getListMedicine().size(); i++) {
			Medicine med = dao.getListMedicine().get(i);
			model.addRow(new Object[] {
					med.getProID(),
					i + 1,
					med.getName(),
					formatMoney(med.getPrice()) ,
					med.getQuantity(),
					med.getType(),
					med.getCreateDate()
			});
		}
		table.setModel(model);
		table.removeColumn(table.getColumnModel().getColumn(0));
		table.getColumnModel().getColumn(0).setMaxWidth(40);
	}
	
	protected void do_txtSearch_keyPressed(KeyEvent e) {
		String find = txtSearch.getText();
		DefaultRowSorter<?, ?> sorter = 
				(DefaultRowSorter<?, ?>) table.getRowSorter();
		sorter.setRowFilter(RowFilter.regexFilter(find));
		sorter.setSortKeys(null);
	}
	protected void do_btnNewButton_actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Please choose image");
		chooser.setFileFilter(new FileNameExtensionFilter("image (jpg, png, gif)","jpg","png","gif"));
		chooser.setAcceptAllFileFilterUsed(false);
		int result = chooser.showOpenDialog(null);
		
		if (result == chooser.APPROVE_OPTION) {
			File f = chooser.getSelectedFile();
			lblIconPicture.setIcon(
					(Icon) new ImageIcon(
					new ImageIcon(f.getAbsolutePath())
						.getImage().getScaledInstance(100, 80, Image.SCALE_SMOOTH)));
			
			String filename = f.getAbsolutePath();
			String newPath = "src/main/resources/Image";
			File directory = new File(newPath);
			if(!directory.exists()) {directory.mkdirs();}
			File sourceFile = null;
			File destination = null;
			String extend = filename.substring(filename.lastIndexOf(".") + 1);
			sourceFile = new File(filename);
			destination = new File(newPath +"/"+txtId.getText()+"."+extend); // path of new image
			try {
				Files.deleteIfExists(Paths.get(newPath +"/"+txtId.getText()+"."+extend));
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			try {
				Files.copy(sourceFile.toPath(), destination.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			txtPicture.setVisible(false);
			txtPicture.setText(destination.toString());
		}
	}
	private boolean validform() {
		if(Validation.Checkempty(txtName, "Have not enter name!") == false) {
			return false;
		}
		if(Validation.Checkempty(txtPrice, "Have not enter price!") == false) {
			return false;
		}
		if(Validation.Checkempty(txtQuantity, "Have not enter quantity!") == false) {
			return false;
		}
		if(Validation.Checkempty(txtBrand, "Have not enter brand!") == false) {
			return false;
		}
		if(Validation.Checkempty(txtUse, "Have not enter uses!") == false) {
			return false;
		}
		if(Validation.Checkempty(txtType, "Have not enter type!") == false) {
			return false;
		}
		if(Validation.CheckRegex(txtPrice,RegexConst.NUMBER, "Price must be number") == false) {
			return false;
		}
		if(Validation.CheckRegex(txtQuantity,RegexConst.NUMBER, "Price must be number") == false) {
			return false;
		}
		return true;
	}

	private String formatMoney(float f) {
		DecimalFormat myDF = new DecimalFormat("0.#");
		return myDF.format(f);
	}
	protected void do_mbtnCopy_actionPerformed(ActionEvent e) {
		txtId.setText("");
		mbtnUpdate.setEnabled(false);
		mbtnDelete.setEnabled(false);
		mbtnAdd.setEnabled(true);
	}
}

