package form;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;

import java.awt.Rectangle;
import component.Card;
import dao.MedicineDao;
import dao.SaleDao;
import dao.StaffDao;
import helper.FormatNumber;

import javax.swing.JLayeredPane;
import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import extra.Table;
import component.MyButton;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Home extends JPanel {
	private JLayeredPane layeredPane;
	private Card card_1;
	private Card card_2;
	private Card card_3;
	private JLabel lblicon;
	private JLabel lblTitle2;
	public static JLabel lblValue2;
	private JLabel lblIcon;
	private JLabel lblTitle;
	public static JLabel lblValue;
	private JLabel lblIcon3;
	private JLabel lblTitle3;
	public static JLabel lblValue3;
	private MedicineDao medDao;
	private SaleDao saleDao;
	private StaffDao staffDao;
	private FormatNumber fn;
	private JScrollPane scrollPane;
	private Table table;
	private MyButton myButton;

	/**
	 * Create the panel.
	 */
	public Home() {
		setBounds(new Rectangle(0, 0, 916, 598));
		setBackground(Color.WHITE);
		layeredPane = new JLayeredPane();
		
		scrollPane = new JScrollPane();
		
		myButton = new MyButton();
		myButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				myButtonMouseClicked(e);
			}
		});
		myButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		myButton.setRadius(10);
		myButton.setBorder(null);
		myButton.setIcon(new ImageIcon(Home.class.getResource("/icon/reload (2).png")));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 888, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(myButton, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(layeredPane, GroupLayout.DEFAULT_SIZE, 879, Short.MAX_VALUE))
							.addGap(19))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addComponent(layeredPane, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
					.addGap(14)
					.addComponent(myButton, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 344, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		table = new Table();
		scrollPane.setViewportView(table);
		layeredPane.setLayout(new GridLayout(0, 3, 20, 0));
		
		card_1 = new Card();
		card_1.setColor2(new Color(55, 59, 68));
		card_1.setColor1(new Color(66, 134, 244));
		layeredPane.add(card_1);
		card_1.setLayout(null);
		
		lblicon = new JLabel("");
		lblicon.setBounds(10, 10, 130, 130);
		lblicon.setHorizontalAlignment(SwingConstants.CENTER);
		lblicon.setIcon(new ImageIcon(Home.class.getResource("/icon/pill (3).png")));
		card_1.add(lblicon);
		
		lblTitle = new JLabel("Medicines");
		lblTitle.setForeground(Color.WHITE);
		lblTitle.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblTitle.setBounds(142, 34, 127, 30);
		card_1.add(lblTitle);
		
		medDao = new MedicineDao();
		lblValue = new JLabel(String.valueOf(medDao.sumProduct()));
		lblValue.setForeground(Color.WHITE);
		lblValue.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblValue.setBounds(142, 74, 127, 30);
		card_1.add(lblValue);
		
		card_2 = new Card();
		card_2.setColor2(new Color(73, 50, 64));
		card_2.setColor1(new Color(255, 0, 153));
		layeredPane.add(card_2);
		
		lblIcon = new JLabel("");
		lblIcon.setIcon(new ImageIcon(Home.class.getResource("/icon/money (4).png")));
		lblIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lblIcon.setBounds(0, 10, 130, 130);
		card_2.add(lblIcon);
		
		
		lblTitle2 = new JLabel("Profit");
		lblTitle2.setBounds(140, 35, 127, 30);
		card_2.add(lblTitle2);
		lblTitle2.setForeground(Color.WHITE);
		lblTitle2.setFont(new Font("SansSerif", Font.BOLD, 18));
		
		saleDao = new SaleDao();
		fn = new FormatNumber();
		lblValue2 = new JLabel(fn.DoubleToString(saleDao.sumProfit()));
		lblValue2.setBounds(140, 75, 127, 30);
		card_2.add(lblValue2);
		lblValue2.setForeground(Color.WHITE);
		lblValue2.setFont(new Font("SansSerif", Font.BOLD, 18));
		
		card_3 = new Card();
		card_3.setColor2(new Color(121, 159, 12));
		card_3.setColor1(new Color(255, 224, 0));
		layeredPane.add(card_3);
		
		lblIcon3 = new JLabel("");
		lblIcon3.setIcon(new ImageIcon(Home.class.getResource("/icon/user (2).png")));
		lblIcon3.setHorizontalAlignment(SwingConstants.CENTER);
		lblIcon3.setBounds(0, 22, 130, 130);
		card_3.add(lblIcon3);
		
		lblTitle3 = new JLabel("Users");
		lblTitle3.setForeground(Color.WHITE);
		lblTitle3.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblTitle3.setBounds(140, 47, 127, 30);
		card_3.add(lblTitle3);
		
		staffDao = new StaffDao();
		lblValue3 = new JLabel(String.valueOf(staffDao.sumStaff()));
		lblValue3.setForeground(Color.WHITE);
		lblValue3.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblValue3.setBounds(140, 87, 127, 30);
		card_3.add(lblValue3);
		setLayout(groupLayout);
		loadMedicine();
	}
	
	public void loadMedicine () {
		MedicineDao dao = new MedicineDao();
		DefaultTableModel model = new DefaultTableModel();
				
		model.addColumn("Id");
		model.addColumn("Name");
		model.addColumn("Price");
		model.addColumn("Quantity");
		model.addColumn("Type");
		model.addColumn("Create date");
		
		
		dao.getListMedicine().forEach(med->model.addRow(new Object[] {
				med.getProID(),
				med.getName(),
				fn.DoubleToString(med.getPrice()),
				med.getQuantity(),
				med.getType(),
				med.getCreateDate()
		}));
		table.setModel(model);
	}
	protected void myButtonMouseClicked(MouseEvent e) {
		loadMedicine ();
	}
}
