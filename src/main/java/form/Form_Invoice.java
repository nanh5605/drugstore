package form;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import component.MyButton;
import dao.SaleDao;
import entity.Invoice;
import extra.Table;
import gui.JframeDashboard;
import form.JframeInvoiceDetail;
import helper.FormatNumber;

import javax.swing.DefaultRowSorter;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Form_Invoice extends JPanel {
	private JScrollPane scrollPane;
	private JLabel lblNewLabel;
	private JTextField txtSearch;
	private MyButton mbtnView;
	private Table tableInvoice;
	private MyButton mbtnRefresh;

	/**
	 * Create the panel.
	 */
	public Form_Invoice() {
		setBackground(Color.WHITE);
		setBounds(0,0,916,588);
		
		scrollPane = new JScrollPane();
		
		tableInvoice = new Table();
		tableInvoice.setAutoCreateRowSorter(true);
		tableInvoice.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tableInvoiceMouseClicked(e);
			}
		});
		scrollPane.setViewportView(tableInvoice);
		loadInvoice();
		
		lblNewLabel = new JLabel("Search:");
		lblNewLabel.setForeground(SystemColor.textHighlight);
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 13));
		
		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				txtSearchKeyPressed(e);
			}
		});
		txtSearch.setOpaque(false);
		txtSearch.setColumns(10);
		txtSearch.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		mbtnView = new MyButton();
		mbtnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnViewActionPerformed(e);
			}
		});
		mbtnView.setText("View");
		mbtnView.setRadius(15);
		mbtnView.setColorOver(new Color(0, 191, 255));
		mbtnView.setColorClick(new Color(100, 149, 237));
		mbtnView.setBorderColor(SystemColor.textHighlight);
		mbtnView.setBorder(null);
		
		mbtnRefresh = new MyButton();
		mbtnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnRefreshActionPerformed(e);
			}
		});
		mbtnRefresh.setText("Refresh");
		mbtnRefresh.setRadius(15);
		mbtnRefresh.setColorOver(new Color(0, 191, 255));
		mbtnRefresh.setColorClick(new Color(100, 149, 237));
		mbtnRefresh.setBorderColor(SystemColor.textHighlight);
		mbtnRefresh.setBorder(null);
		
		mbtnExport = new MyButton();
		mbtnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnExportActionPerformed(e);
			}
		});
		mbtnExport.setText("Export");
		mbtnExport.setRadius(15);
		mbtnExport.setColorOver(new Color(0, 191, 255));
		mbtnExport.setColorClick(new Color(100, 149, 237));
		mbtnExport.setBorderColor(SystemColor.textHighlight);
		mbtnExport.setBorder(null);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
							.addGap(152)
							.addComponent(mbtnView, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addGap(33)
							.addComponent(mbtnRefresh, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addGap(29)
							.addComponent(mbtnExport, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 876, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(20, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(29)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(mbtnView, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnRefresh, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnExport, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(37)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
					.addGap(10)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 509, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		setLayout(groupLayout);
		setVisible(true);
	}
	
	protected void loadInvoice() {
		FormatNumber fn = new FormatNumber();
		SaleDao sale = new SaleDao();
		DefaultTableModel model = new DefaultTableModel();
	
		model.addColumn("ID");
		model.addColumn("Date");
		model.addColumn("Total");
		model.addColumn("Staff");
		
		sale.getListInvoice().forEach(inv -> model.addRow(new Object[] 
				{inv.getInvoiceId(), inv.getDateOfCreate(),fn.DoubleToString(inv.getTotal()),sale.selectStaffById(inv.getStaffId())}
		));
		
		tableInvoice.setModel(model);
	}
	protected void mbtnRefreshActionPerformed(ActionEvent e) {
		loadInvoice();
	}
	public static String invoiceID;
	private MyButton mbtnExport;
	protected void tableInvoiceMouseClicked(MouseEvent e) {
		int rowIndex = tableInvoice.getSelectedRow();
		invoiceID = tableInvoice.getValueAt(rowIndex, 0).toString();
	}
	
	protected void mbtnViewActionPerformed(ActionEvent e) {
		JframeInvoiceDetail inde = new JframeInvoiceDetail();
		inde.setLocationRelativeTo(this);
		inde.setVisible(true);

	}

	protected void txtSearchKeyPressed(KeyEvent e) {
		String find = txtSearch.getText();
		DefaultRowSorter<?, ?> sorter = 
				(DefaultRowSorter<?, ?>) tableInvoice.getRowSorter();
		sorter.setRowFilter(RowFilter.regexFilter(find));
		sorter.setSortKeys(null);
	}
	protected void mbtnExportActionPerformed(ActionEvent e) {
		InvoiceReport invoice = new InvoiceReport();
		invoice.setVisible(true);
	}
}
