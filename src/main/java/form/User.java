package form;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import extra.Table;
import helper.RegexConst;
import helper.Validation;
import component.MyButton;
import dao.AccountDao;
import dao.StaffDao;
import entity.Staff;

import javax.swing.JTextPane;
import javax.swing.RowFilter;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.Image;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import java.awt.Rectangle;
import javax.swing.LayoutStyle.ComponentPlacement;
import com.toedter.calendar.JDateChooser;

import common.ConnectDB;

import javax.swing.JTextArea;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultRowSorter;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class User extends JPanel {
	private JLabel lblIID;
	private JLabel lblFullname;
	private JLabel lblBirthday;
	private JLabel lblUserID;
	private JLabel lblIphone;
	private JLabel lblAddress;
	private JLabel lblStatus;
	private JLabel lblGender;
	private JLabel lblPicture;
	private MyButton mbtnClear;
	private MyButton mbtnDelete;
	private MyButton mbtnUpdate;
	private MyButton mbtnAdd;
	private JScrollPane scrollPane;
	private Table tableUser;
	private JComboBox comboBoxGender;
	private JLabel lblAccountID;
	private JTextField txtID;
	private JTextField txtSearch;
	private JLabel lblSearch;
	private JTextField txtfullname;
	private JDateChooser dateChooserdob;
	private JTextField txtPhone;
	private JTextArea textAreaAddress;
	private JTextField txtUserID;
	private JComboBox comboBoxStatus;
	private JTextField txtAccID;
	private MyButton mbtnImportHere;
	private JTextField txtpicture;
	private JLabel lblUsername;
	private JComboBox comboBoxUsername;
	private JLabel lblAvatar;

	/**
	 * Create the panel.
	 */
	public User() {
		setForeground(SystemColor.textHighlight);
		setBackground(Color.WHITE);
		setBounds(0,0,916,588);
		setVisible(true);
		
		lblIID = new JLabel("ID");
		lblIID.setFont(new Font("Arial", Font.BOLD, 12));
		lblIID.setForeground(SystemColor.textHighlight);
		
		lblFullname = new JLabel("Fullname");
		lblFullname.setFont(new Font("Arial", Font.BOLD, 12));
		lblFullname.setForeground(SystemColor.textHighlight);
		
		lblBirthday = new JLabel("Birthday");
		lblBirthday.setFont(new Font("Arial", Font.BOLD, 12));
		lblBirthday.setForeground(SystemColor.textHighlight);
		
		lblUserID = new JLabel("UserID");
		lblUserID.setFont(new Font("Arial", Font.BOLD, 12));
		lblUserID.setForeground(SystemColor.textHighlight);
		
		lblIphone = new JLabel("Phone");
		lblIphone.setFont(new Font("Arial", Font.BOLD, 12));
		lblIphone.setForeground(SystemColor.textHighlight);
		
		lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Arial", Font.BOLD, 12));
		lblAddress.setForeground(SystemColor.textHighlight);
		
		lblStatus = new JLabel("Status");
		lblStatus.setFont(new Font("Arial", Font.BOLD, 12));
		lblStatus.setForeground(SystemColor.textHighlight);
		
		lblGender = new JLabel("Gender");
		lblGender.setFont(new Font("Arial", Font.BOLD, 12));
		lblGender.setForeground(SystemColor.textHighlight);
		
		lblPicture = new JLabel("Picture");
		lblPicture.setFont(new Font("Arial", Font.BOLD, 12));
		lblPicture.setForeground(SystemColor.textHighlight);
		
		mbtnClear = new MyButton();
		mbtnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnClearActionPerformed(e);
			}
		});
		mbtnClear.setText("Clear");
		mbtnClear.setRadius(15);
		mbtnClear.setColorOver(new Color(0, 191, 255));
		mbtnClear.setColorClick(new Color(100, 149, 237));
		mbtnClear.setBorderColor(SystemColor.textHighlight);
		mbtnClear.setBorder(null);
		
		mbtnDelete = new MyButton();
		mbtnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnDeleteActionPerformed(e);
			}
		});
		mbtnDelete.setText("Delete");
		mbtnDelete.setRadius(15);
		mbtnDelete.setColorOver(new Color(0, 191, 255));
		mbtnDelete.setColorClick(new Color(100, 149, 237));
		mbtnDelete.setBorderColor(SystemColor.textHighlight);
		mbtnDelete.setBorder(null);
		
		mbtnUpdate = new MyButton();
		mbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnUpdateActionPerformed(e);
			}
		});
		mbtnUpdate.setText("Update");
		mbtnUpdate.setRadius(15);
		mbtnUpdate.setColorOver(new Color(0, 191, 255));
		mbtnUpdate.setColorClick(new Color(100, 149, 237));
		mbtnUpdate.setBorderColor(SystemColor.textHighlight);
		mbtnUpdate.setBorder(null);
		
		mbtnAdd = new MyButton();
		mbtnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnAddActionPerformed(e);
			}
		});
		mbtnAdd.setText("Add");
		mbtnAdd.setRadius(15);
		mbtnAdd.setColorOver(new Color(0, 191, 255));
		mbtnAdd.setColorClick(new Color(100, 149, 237));
		mbtnAdd.setBorderColor(SystemColor.textHighlight);
		mbtnAdd.setBorder(null);
		
		scrollPane = new JScrollPane();
		
		tableUser = new Table();
		tableUser.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tableUserMouseClicked(e);
			}
		});
		tableUser.setAutoCreateRowSorter(true);
		scrollPane.setViewportView(tableUser);
		
		comboBoxGender = new JComboBox();
		comboBoxGender.setModel(new DefaultComboBoxModel(new String[] {"Male", "Female"}));
		
		lblAccountID = new JLabel("AccountID");
		lblAccountID.setForeground(SystemColor.textHighlight);
		lblAccountID.setFont(new Font("Arial", Font.BOLD, 12));
		
		txtID = new JTextField();
		txtID.setEditable(false);
		txtID.setOpaque(false);
		txtID.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtID.setFont(new Font("SansSerif", Font.PLAIN, 13));
		txtID.setColumns(10);
		
		txtSearch = new JTextField();
		txtSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtSearchActionPerformed(e);
			}
		});
		txtSearch.setOpaque(false);
		txtSearch.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtSearch.setColumns(10);
		
		lblSearch = new JLabel("Search:");
		lblSearch.setForeground(SystemColor.textHighlight);
		lblSearch.setFont(new Font("SansSerif", Font.BOLD, 14));
		
		txtfullname = new JTextField();
		txtfullname.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtfullname.setFont(new Font("SansSerif", Font.PLAIN, 13));
		txtfullname.setOpaque(false);
		txtfullname.setColumns(10);
		
		dateChooserdob = new JDateChooser();
		dateChooserdob.setDate(new Date());
		dateChooserdob.setDateFormatString("yyyy-MM-dd");
		
		txtPhone = new JTextField();
		txtPhone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtPhoneActionPerformed(e);
			}
		});
		txtPhone.setOpaque(false);
		txtPhone.setFont(new Font("SansSerif", Font.PLAIN, 13));
		txtPhone.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtPhone.setColumns(10);
		
		textAreaAddress = new JTextArea();
		textAreaAddress.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		txtUserID = new JTextField();
		txtUserID.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtUserID.setFont(new Font("SansSerif", Font.PLAIN, 13));
		txtUserID.setOpaque(false);
		txtUserID.setColumns(10);
		
		comboBoxStatus = new JComboBox();
		comboBoxStatus.setModel(new DefaultComboBoxModel(new String[] {"Working", "Has retired"}));
		
		txtAccID = new JTextField();
		txtAccID.setEditable(false);
		txtAccID.setOpaque(false);
		txtAccID.setFont(new Font("SansSerif", Font.PLAIN, 13));
		txtAccID.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtAccID.setColumns(10);
		
		mbtnImportHere = new MyButton();
		mbtnImportHere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnImportHereActionPerformed(e);
			}
		});
		mbtnImportHere.setBorderColor(SystemColor.textHighlight);
		mbtnImportHere.setFont(new Font("SansSerif", Font.PLAIN, 12));
		mbtnImportHere.setText("Import");
		mbtnImportHere.setRadius(5);
		
		txtpicture = new JTextField();
		txtpicture.setOpaque(false);
		txtpicture.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtpicture.setColumns(10);
		
		lblUsername = new JLabel("Username");
		lblUsername.setForeground(SystemColor.textHighlight);
		lblUsername.setFont(new Font("Arial", Font.BOLD, 12));
		
		comboBoxUsername = new JComboBox();
		AccountDao dao = new AccountDao();
		dao.showUsername().forEach(acc -> comboBoxUsername.addItem(acc.getUsername()));
		comboBoxUsername.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxUsernameActionPerformed(e);
			}
		});
		
		lblAvatar = new JLabel(new ImageIcon(new ImageIcon(User.class
				.getResource("/img/noImg.png"))
				.getImage().getScaledInstance(100,80, Image.SCALE_SMOOTH)));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblIID, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(txtID, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(68)
									.addComponent(comboBoxUsername, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))
							.addGap(14)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblFullname, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(58)
									.addComponent(txtfullname, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblBirthday, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(58)
									.addComponent(dateChooserdob, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(50)
									.addComponent(txtPhone, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblIphone, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblUserID, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(50)
									.addComponent(txtUserID, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAddress, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(55)
									.addComponent(textAreaAddress, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAccountID, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(68)
									.addComponent(txtAccID, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))
							.addGap(14)
							.addComponent(lblGender, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
							.addGap(2)
							.addComponent(comboBoxGender, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(lblStatus, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
							.addGap(2)
							.addComponent(comboBoxStatus, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(lblPicture, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
							.addGap(7)
							.addComponent(txtpicture, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(mbtnImportHere, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(40)
									.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(50)
									.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(50)
									.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblSearch, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
									.addGap(10)
									.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)))
							.addGap(50)
							.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addGap(100)
							.addComponent(lblAvatar, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 896, GroupLayout.PREFERRED_SIZE)))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(13)
									.addComponent(lblIID))
								.addComponent(txtID, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(22)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(6)
									.addComponent(lblUsername))
								.addComponent(comboBoxUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(13)
									.addComponent(lblFullname))
								.addComponent(txtfullname, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(22)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(6)
									.addComponent(lblBirthday))
								.addComponent(dateChooserdob, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(txtPhone, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(13)
									.addComponent(lblIphone)))
							.addGap(22)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(6)
									.addComponent(lblUserID))
								.addComponent(txtUserID, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(13)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAddress)
								.addComponent(textAreaAddress, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))))
					.addGap(17)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(11)
							.addComponent(lblAccountID))
						.addComponent(txtAccID, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(11)
							.addComponent(lblGender))
						.addComponent(comboBoxGender, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(11)
							.addComponent(lblStatus))
						.addComponent(comboBoxStatus, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(11)
							.addComponent(lblPicture))
						.addComponent(txtpicture, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(mbtnImportHere, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(15)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
							.addGap(12)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(1)
									.addComponent(lblSearch))
								.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(15)
							.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
						.addComponent(lblAvatar, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
					.addGap(15)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 346, GroupLayout.PREFERRED_SIZE))
		);
		setLayout(groupLayout);
		loaddatauser();
	}

	private void loaddatauser() {
		DefaultTableModel model = new DefaultTableModel();
		StaffDao staffdao = new StaffDao();
		AccountDao dao = new AccountDao();
		model.addColumn("STT");
		model.addColumn("Name");
		model.addColumn("Birthday");
		model.addColumn("Gender");
		model.addColumn("identifierCode");
		model.addColumn("username");
		model.addColumn("status");
		
		staffdao.getListStaff().forEach(sta -> model.addRow(new Object[] {
				sta.getStaffId(),
				sta.getFullname(),sta.getDob(),
				sta.isGender()?"Male":"Female",
				sta.getIdentifierCode(),
				dao.getusername(sta.getAccId()),sta.isStatus()?"Working":"Has retired"
		}));
		
		tableUser.setModel(model);
	}

	protected void comboBoxUsernameActionPerformed(ActionEvent e) {
		if(comboBoxUsername.getItemCount() != 0){
			AccountDao dao = new AccountDao();
			int i = dao.showAccId(comboBoxUsername.getSelectedItem().toString());
			txtAccID.setText(Integer.toString(i));
		}	
	}
	private boolean validform() {
		if(Validation.Checkempty(txtfullname, "Have not entered Fullname!") == false) {
			JOptionPane.showMessageDialog(this, "Please enter Fullname");
			return false;
		}
		if(Validation.CheckRegex(txtfullname,RegexConst.CHARS, "Fullname is not correct format") == false) {
			return false;
		}
		if(Validation.Checkempty(txtPhone, "Have not entered Phone!") == false) {
			JOptionPane.showMessageDialog(this, "Please enter Phone");
			return false;
		}
		if(Validation.CheckRegex(txtPhone, RegexConst.PHONE, "Phone not valid") == false) {
			return false;
		}
		if(Validation.Checkempty(txtUserID, "Have not entered UserID!") == false) {
			JOptionPane.showMessageDialog(this, "Please enter UserID");
			return false;
		}
		if(Validation.CheckRegex(txtUserID, RegexConst.USERID, "UserID must be 6 number") == false) {
			return false;
		}
		String address = textAreaAddress.getText().trim();
		if(address.equals("")) {
			JOptionPane.showMessageDialog(this, "Please enter Address");
			return false;
		}
		if((2022-(dateChooserdob.getDate().getYear()+1900) ) <= 18) {
			JOptionPane.showMessageDialog(this, "dob not valid");
			return false;
		}
		if(Validation.Checkempty(txtpicture, "Picture is Empty!") == false) {
			JOptionPane.showMessageDialog(this, "Please choose picture");
			return false;
		}
		return true;
	}
	protected void mbtnAddActionPerformed(ActionEvent e) {
		StaffDao stadao = new StaffDao();
		Staff sta = new Staff();
		if(validform() == false) {
			return;
		}
		sta.setFullname(txtfullname.getText());
		sta.setDob(LocalDate.ofInstant(dateChooserdob.getDate().toInstant(), ZoneId.systemDefault()));
		sta.setGender(comboBoxGender.getSelectedItem().toString().equals("Male")?true:false);
		sta.setIdentifierCode(Integer.parseInt(txtUserID.getText()));
		sta.setPhone(txtPhone.getText());
		sta.setAddress(textAreaAddress.getText());
		sta.setAccId(Integer.parseInt(txtAccID.getText()));
		sta.setPicture(txtpicture.getText());
		sta.setStatus(comboBoxStatus.getSelectedItem().toString().equals("Working")?true:false);
		if(stadao.checkexistStaff(Integer.parseInt(txtAccID.getText()),Integer.parseInt(txtUserID.getText())) > 0) {
			JOptionPane.showMessageDialog(this, "AccId or IdentifierCode is Exist");
			return;
		}
		if(stadao.AddStaff(sta) > 0) {
			loaddatauser();
			JOptionPane.showMessageDialog(this, "Success");
			Home.lblValue3.setText(String.valueOf(stadao.sumStaff()));
			clear();
		}
		
	}
	
	protected void mbtnUpdateActionPerformed(ActionEvent e) {
		StaffDao stadao = new StaffDao();
		Staff sta = new Staff();
		if(Validation.CheckRegex(txtID, RegexConst.NUMBER, "AccID must be number") == false) {
			return;
		}
		if(validform() == false) {
			return;
		}
		sta.setStaffId(Integer.parseInt(txtID.getText()));
		sta.setFullname(txtfullname.getText());
		sta.setDob(LocalDate.ofInstant(dateChooserdob.getDate().toInstant(), ZoneId.systemDefault()));
		sta.setGender(comboBoxGender.getSelectedItem().toString().equals("Male")?true:false);
		sta.setIdentifierCode(Integer.parseInt(txtUserID.getText()));
		sta.setPhone(txtPhone.getText());
		sta.setAddress(textAreaAddress.getText());
		sta.setPicture(txtpicture.getText());
		sta.setStatus(comboBoxStatus.getSelectedItem().toString().equals("Working")?true:false);
		stadao.UpdateStaff(sta);
		loaddatauser();
		JOptionPane.showMessageDialog(this, "Update Success");
		Home.lblValue3.setText(String.valueOf(stadao.sumStaff()));
		clear();
	}
	
	protected void mbtnDeleteActionPerformed(ActionEvent e) {
		StaffDao stadao = new StaffDao();
		Staff sta = new Staff();
		if(txtfullname.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Nothing to delete");
		}
		else {
			sta.setStaffId(Integer.parseInt(txtID.getText()));
			String str = txtfullname.getText();
			if(JOptionPane.showConfirmDialog(this, "Are you sure want to delete "+ str + "?","DELETE",
					JOptionPane.YES_NO_OPTION) == 0) {
				try {
					Files.deleteIfExists(Paths.get(txtpicture.getText()));
				} catch (IOException e2) {
					e2.printStackTrace();
				}
				stadao.DelStaff(sta);
				clear();
				loaddatauser();
				JOptionPane.showMessageDialog(this, "Delete success");
				Home.lblValue3.setText(String.valueOf(stadao.sumStaff()));
			}
		}
	}
	
	protected void mbtnClearActionPerformed(ActionEvent e) {
		clear();
		if(comboBoxUsername.getItemCount() != 0){
			comboBoxUsername.removeAllItems(); 
		}
		AccountDao dao = new AccountDao();
		dao.showUsername().forEach(acc -> comboBoxUsername.addItem(acc.getUsername()));
		loaddatauser();
	}

	private void clear() {
		txtID.setText("");
		txtfullname.setText(""); 
		txtPhone.setText("");
		txtUserID.setText("");
		txtAccID.setText("");
		txtpicture.setText("");
		lblAvatar.setIcon(
				new ImageIcon(
				new ImageIcon("src/main/resources/img/noImg.png")
					.getImage().getScaledInstance(100, 80, Image.SCALE_SMOOTH)));
		dateChooserdob.setDate(new Date());
		textAreaAddress.setText("");
		mbtnAdd.setVisible(true);
	}
	protected void mbtnImportHereActionPerformed(ActionEvent e) {
		if(txtUserID.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Fullname not entered");
			return;
		}
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("please choose image");
		chooser.setFileFilter(new FileNameExtensionFilter("image (jpg, png, gif)","jpg","png","gif"));
		chooser.setAcceptAllFileFilterUsed(false);//đúng tên file ở trên mới cho mở
		int result = chooser.showOpenDialog(null);
		
		if (result == chooser.APPROVE_OPTION) {
			File f = chooser.getSelectedFile();
			lblAvatar.setIcon(
					new ImageIcon(
					new ImageIcon(f.getAbsolutePath())
						.getImage().getScaledInstance(100, 80, Image.SCALE_SMOOTH)));
			
			String filename = f.getAbsolutePath();
			String newPath = "src/main/resources/Image";
			File directory = new File(newPath);
			if(!directory.exists()) {directory.mkdirs();}
			File sourceFile = null;
			File destination = null;
			String extend = filename.substring(filename.lastIndexOf(".") + 1);
			sourceFile = new File(filename);
			destination = new File(newPath +"/"+txtUserID.getText()+"."+extend);
			try {
				Files.deleteIfExists(Paths.get(newPath +"/"+txtUserID.getText()+"."+extend));
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			try {
				Files.copy(sourceFile.toPath(), destination.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			txtpicture.setText(destination.toString());
		}
	}
	protected void tableUserMouseClicked(MouseEvent e) {
		comboBoxUsername.setEditable(true);
		mbtnAdd.setVisible(false);
		int rowIndex = tableUser.getSelectedRow();
		txtID.setText(tableUser.getValueAt(rowIndex, 0).toString());
		txtfullname.setText(tableUser.getValueAt(rowIndex, 1).toString());
		try {
			dateChooserdob.setDate(new SimpleDateFormat("yyyy-MM-dd")
					.parse(tableUser.getValueAt(rowIndex, 2).toString()));
		} catch (Exception e2) {
			JOptionPane.showMessageDialog(this, e2.getMessage());
		}
		if(tableUser.getValueAt(rowIndex, 3).toString().equals("Male")) {
			comboBoxGender.setSelectedItem("Male");
		}else { comboBoxGender.setSelectedItem("Female");}
		txtUserID.setText(tableUser.getValueAt(rowIndex, 4).toString());
		comboBoxUsername.setSelectedItem(tableUser.getValueAt(rowIndex, 5).toString());
		if(tableUser.getValueAt(rowIndex, 6).toString().equals("Working")) {
			comboBoxGender.setSelectedItem("Working");
		}else { comboBoxGender.setSelectedItem("Has retired");}
		StaffDao stadao = new StaffDao();
		stadao.getListStaff(Integer.parseInt(tableUser.getValueAt(rowIndex, 4).toString()))
		.forEach(sta -> txtPhone.setText(sta.getPhone()));
		stadao.getListStaff(Integer.parseInt(tableUser.getValueAt(rowIndex, 4).toString()))
		.forEach(sta -> textAreaAddress.setText(sta.getAddress()));
		stadao.getListStaff(Integer.parseInt(tableUser.getValueAt(rowIndex, 4).toString()))
		.forEach(sta -> txtpicture.setText(sta.getPicture()));
		lblAvatar.setIcon(
				new ImageIcon(
				new ImageIcon(txtpicture.getText())
					.getImage().getScaledInstance(100, 80, Image.SCALE_SMOOTH)));
	}
	protected void txtSearchActionPerformed(ActionEvent e) {
		if(txtSearch.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Please enter Staff want to find");
		}else {
			String find = txtSearch.getText();
			DefaultRowSorter<?, ?> sorter = (DefaultRowSorter<?,?>) tableUser.getRowSorter();
			sorter.setRowFilter(RowFilter.regexFilter(find));
			sorter.setSortKeys(null);
		}
	}
	protected void txtPhoneActionPerformed(ActionEvent e) {
		int user1 = dateChooserdob.getDate().getYear() + 1900;
		if(txtPhone.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Phone not entered!!!!");
		}else {
			String user2 = txtPhone.getText().substring(6,10);
			txtUserID.setText(Integer.toString(user1) + user2);
		}
	}
}
