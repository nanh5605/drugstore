package form;

import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.JPanel;
import javax.swing.DefaultRowSorter;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import extra.Table;
import helper.RegexConst;
import helper.Validation;

import javax.swing.JScrollPane;
import component.MyButton;
import dao.CategoryDao;
import dao.MedicineDao;
import dao.ProviderDao;
import entity.Categories;
import entity.Medicine;

import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.MatteBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Category extends JPanel {
	private JScrollPane scrollPane;
	private Table table;
	private MyButton mbtnAdd;
	private MyButton mbtnUpdate;
	private MyButton mbtnDelete;
	private MyButton mbtnClear;
	private JLabel lblSearch;
	private JTextField txtSearch;
	private JLabel lblCateId;
	private JLabel lblCategoryname;
	private JLabel lblStatus;
	private JTextField txtName;
	private JTextField txtId;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnOn;
	private JRadioButton rdbtnOff;

	/**
	 * Create the panel.
	 */
	public Category() {
		setForeground(SystemColor.textHighlight);
		setBackground(Color.WHITE);
		setBounds(0,0,916,588);
		
		scrollPane = new JScrollPane();
		
		table = new Table();
		CategoryDao dao = new CategoryDao();
		loadCategory(dao);
		
		table.setAutoCreateRowSorter(true);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tableMouseClicked(e);
			}
		});
		scrollPane.setViewportView(table);
		
		mbtnAdd = new MyButton();
		mbtnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnAdd_actionPerformed(e);
			}
		});
		mbtnAdd.setText("Add");
		mbtnAdd.setRadius(15);
		mbtnAdd.setColorOver(new Color(0, 191, 255));
		mbtnAdd.setColorClick(new Color(100, 149, 237));
		mbtnAdd.setBorderColor(SystemColor.textHighlight);
		mbtnAdd.setBorder(null);
		
		mbtnUpdate = new MyButton();
		mbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnUpdate_actionPerformed(e);
			}
		});
		mbtnUpdate.setText("Update");
		mbtnUpdate.setRadius(15);
		mbtnUpdate.setColorOver(new Color(0, 191, 255));
		mbtnUpdate.setColorClick(new Color(100, 149, 237));
		mbtnUpdate.setBorderColor(SystemColor.textHighlight);
		mbtnUpdate.setBorder(null);
		
		mbtnDelete = new MyButton();
		mbtnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnDelete_actionPerformed(e);
			}
		});
		mbtnDelete.setText("Delete");
		mbtnDelete.setRadius(15);
		mbtnDelete.setColorOver(new Color(0, 191, 255));
		mbtnDelete.setColorClick(new Color(100, 149, 237));
		mbtnDelete.setBorderColor(SystemColor.textHighlight);
		mbtnDelete.setBorder(null);
		
		mbtnClear = new MyButton();
		mbtnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnClear_actionPerformed(e);
			}
		});
		mbtnClear.setText("Clear");
		mbtnClear.setRadius(15);
		mbtnClear.setColorOver(new Color(0, 191, 255));
		mbtnClear.setColorClick(new Color(100, 149, 237));
		mbtnClear.setBorderColor(SystemColor.textHighlight);
		mbtnClear.setBorder(null);
		
		lblSearch = new JLabel("Search:");
		lblSearch.setForeground(SystemColor.textHighlight);
		lblSearch.setFont(new Font("SansSerif", Font.BOLD, 13));
		
		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				do_txtSearch_keyPressed(e);
			}
		});
		txtSearch.setOpaque(false);
		txtSearch.setColumns(10);
		txtSearch.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		lblCateId = new JLabel("ID");
		lblCateId.setForeground(SystemColor.textHighlight);
		lblCateId.setFont(new Font("Arial", Font.BOLD, 12));
		
		lblCategoryname = new JLabel("Category Name");
		lblCategoryname.setForeground(SystemColor.textHighlight);
		lblCategoryname.setFont(new Font("Arial", Font.BOLD, 12));
		
		lblStatus = new JLabel("Status");
		lblStatus.setForeground(SystemColor.textHighlight);
		lblStatus.setFont(new Font("Arial", Font.BOLD, 12));
		
		txtName = new JTextField();
		txtName.setOpaque(false);
		txtName.setColumns(10);
		txtName.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		txtId = new JTextField();
		txtId.setEditable(false);
		txtId.setOpaque(false);
		txtId.setColumns(10);
		txtId.setBorder(new MatteBorder(0, 0, 1, 0, (Color) Color.BLACK));
		
		rdbtnOn = new JRadioButton("ON");
		rdbtnOn.setSelected(true);
		buttonGroup.add(rdbtnOn);
		
		rdbtnOff = new JRadioButton("OFF");
		
		buttonGroup.add(rdbtnOff);
		rdbtnOn.setBackground(Color.WHITE);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(57)
							.addComponent(lblCateId, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(txtId, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(46)
							.addComponent(lblCategoryname, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(txtName, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
							.addGap(44)
							.addComponent(lblStatus, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
							.addGap(30)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(rdbtnOn, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
								.addComponent(rdbtnOff, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(41)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(51)
									.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(43)
									.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(45)
									.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblSearch, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(25)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 864, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(27, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(33)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCateId)
						.addComponent(txtId, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(lblCategoryname))
						.addComponent(txtName, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblStatus)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(3)
							.addComponent(rdbtnOn)
							.addGap(10)
							.addComponent(rdbtnOff)))
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblSearch, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(35)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 341, GroupLayout.PREFERRED_SIZE)
					.addGap(34))
		);
		setLayout(groupLayout);
		setVisible(true);
	}
	protected void tableMouseClicked (MouseEvent e) {
		int rowIndex = table.getSelectedRow();
		CategoryDao dao = new CategoryDao();
		mbtnAdd.setVisible(false);
		txtId.setText(table.getValueAt(rowIndex, 0).toString());
		var id = table.getValueAt(rowIndex, 0).toString();
		
		dao.getListCategory().forEach(x-> {if(x.getCateID() == Integer.parseInt(id)) {
			txtId.setText(String.valueOf(x.getCateID()));
			txtName.setText(x.getName());
			if (x.isStatus()== Boolean.parseBoolean("true") ) 
				rdbtnOn.setSelected(true);
			else 
				rdbtnOff.setSelected(true);
		}});
	}
	private void loadCategory(CategoryDao dao) {
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("Id");
		model.addColumn("Name");
		model.addColumn("Status");
		dao.getListCategory().forEach(cate->model.addRow(new Object[] {
				cate.getCateID(),
				cate.getName(),
				cate.isStatus()== true?"In Use":"Not in Use"
		}));
		table.setModel(model);
	}
	private void clearText() {
		txtId.setText("");
		txtName.setText("");
		rdbtnOn.setSelected(true);
		rdbtnOff.setSelected(false);
		mbtnAdd.setVisible(true);
	}
	private boolean validform() {
		if(Validation.Checkempty(txtName, "Have not entered category name!") == false) {
			return false;
		}
		
		return true;
	}
	protected void do_txtSearch_keyPressed(KeyEvent e) {
		String find = txtSearch.getText();
		DefaultRowSorter<?, ?> sorter = 
				(DefaultRowSorter<?, ?>) table.getRowSorter();
		sorter.setRowFilter(RowFilter.regexFilter(find));
		sorter.setSortKeys(null);
	}
	
	
	protected void do_mbtnAdd_actionPerformed(ActionEvent e) {
		CategoryDao dao = new CategoryDao();
		Categories cate = new Categories();
		if(validform() == false) {
			return;
		}
		if (dao.checkNameExist(txtName.getText()).isEmpty()) {
			System.out.println("sdjskaj");
			cate.setName(txtName.getText());
			if (rdbtnOn.isSelected()) {
				cate.setStatus(true);
			}
			dao.insertCategory(cate);
			JOptionPane.showMessageDialog(this, "Add successfully!");
			loadCategory(dao);		
			clearText();
		} else {
			JOptionPane.showMessageDialog(this, "Category is exist");
			
		}
		
	}
	protected void do_mbtnUpdate_actionPerformed(ActionEvent e) {
		if(txtId.getText().equals("")){
			JOptionPane.showMessageDialog(this, "Please choose provider you want to update!");
			return;
		}
		if(validform() == false) {
			return;
		}
		CategoryDao dao = new CategoryDao();
		Categories cate = new Categories();
		
		cate.setName(txtName.getText());
		if (rdbtnOn.isSelected()) {
			cate.setStatus(true);
		}
		cate.setCateID(Integer.parseInt(txtId.getText()));
		
		dao.updateMedicine(cate);
		JOptionPane.showMessageDialog(this, "Update successfully");
		loadCategory(dao);	
		clearText();
	}
	protected void do_mbtnDelete_actionPerformed(ActionEvent e) {
		CategoryDao dao = new CategoryDao();
		if(txtId.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Nothing to delete");
			return;
		} 
		if(JOptionPane.showConfirmDialog(this, "Are you sure want to delete?", "DELETE",
				JOptionPane.YES_NO_OPTION) == 0) {
			int rowIndex = table.getSelectedRow();
			txtId.setText(table.getValueAt(rowIndex, 0).toString());
			int id = (int) table.getValueAt(rowIndex, 0);
			
			dao.deleteCategory(id);
			JOptionPane.showMessageDialog(this, "Delete successfully");
			loadCategory(dao);
			clearText();
		}
		
	}
	protected void do_mbtnClear_actionPerformed(ActionEvent e) {
		clearText();
	}
}
