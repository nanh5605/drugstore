package form;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JScrollPane;
import java.awt.SystemColor;
import component.MyButton;
import dao.SaleDao;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import extra.Table;
import form.Form_Invoice;
import helper.FormatNumber;

import javax.swing.JTextField;

public class JframeInvoiceDetail extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JLabel lblNewLabel;
	private MyButton mbtnClose;
	private Table tableDetail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JframeInvoiceDetail frame = new JframeInvoiceDetail();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JframeInvoiceDetail() {
		setBackground(SystemColor.text);
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 604, 397);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 75, 584, 256);
		contentPane.add(scrollPane);
		
		tableDetail = new Table();
		scrollPane.setViewportView(tableDetail);
//		SaleDao sale = new SaleDao();
//		DefaultTableModel model = new DefaultTableModel();
//		
//		model.addColumn("ID");
//		model.addColumn("Date");
//		model.addColumn("Total");
//		model.addColumn("Staff");
//		
//		sale.getListInvoice().forEach(inv -> model.addRow(new Object[] 
//				{inv.getInvoiceId(), inv.getDateOfCreate(),100000 ,sale.selectStaffById(inv.getStaffId())}
//		));
//		
//		tableDetail.setModel(model);
//		loadInvoice();
		showInvoiceDetail();
		
		lblNewLabel = new JLabel("INVOCE DETAILS");
		lblNewLabel.setForeground(SystemColor.textHighlight);
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblNewLabel.setBounds(10, 10, 259, 43);
		contentPane.add(lblNewLabel);
		
		mbtnClose = new MyButton();
		mbtnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnCloseActionPerformed(e);
			}
		});
		mbtnClose.setText("Close");
		mbtnClose.setRadius(15);
		mbtnClose.setColorOver(new Color(0, 191, 255));
		mbtnClose.setColorClick(new Color(100, 149, 237));
		mbtnClose.setBorderColor(SystemColor.textHighlight);
		mbtnClose.setBorder(null);
		mbtnClose.setBounds(509, 341, 70, 30);
		contentPane.add(mbtnClose);
		
	}
	protected void mbtnCloseActionPerformed(ActionEvent e) {
		this.dispose();
	}
	private int i = 1;
	private double price;
	private int quantity;
	private double total;
	private JTextField textField;
	
	private void showInvoiceDetail() {
		FormatNumber fn = new FormatNumber();
		SaleDao sale = new SaleDao();
		Form_Invoice inv = new Form_Invoice();
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("STT");
		model.addColumn("Name");
		model.addColumn("Unit");
		model.addColumn("Price");
		model.addColumn("Quantity");
		model.addColumn("Total");
		
		
		
		sale.getListInvoiceDetail(Form_Invoice.invoiceID).forEach(ind -> model.addRow(new Object[] 
				{
						i++,
						ind.getProName(),
						ind.getUnit(),
						fn.DoubleToString(price = ind.getPrice()),
						quantity = ind.getQuantity(),
						fn.DoubleToString(total = price * quantity)
				}
		));
 
		tableDetail.setModel(model);
	}
}
