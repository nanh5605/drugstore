package form;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import component.MyButton;
import dao.AccountDao;
import extra.Table;
import helper.RegexConst;
import helper.Validation;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultRowSorter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Account extends JPanel {
	private JScrollPane scrollPane;
	private JLabel lblID;
	private JTextField txtID;
	private MyButton mbtnAdd;
	private MyButton mbtnUpdate;
	private MyButton mbtnDelete;
	private MyButton mbtnClear;
	private Table tableAccount;
	private JLabel lblLevel;
	private JComboBox comboBoxLevel;
	private JLabel lblUsername;
	private JTextField txtUsername;
	private JLabel lblPassword;
	private JTextField txtpassword;
	private JLabel lblSearch;
	private JTextField txtSearch;

	/**
	 * Create the panel.
	 */
	public Account() {
		setBackground(Color.WHITE);
		setBounds(0,0,916,588);
		
		scrollPane = new JScrollPane();
		
		tableAccount = new Table();
		tableAccount.setAutoCreateRowSorter(true);
		tableAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tableAccountMouseClicked(e);
			}
		});
		scrollPane.setViewportView(tableAccount);
		
		lblID = new JLabel("ID");
		lblID.setForeground(SystemColor.textHighlight);
		lblID.setFont(new Font("Arial", Font.BOLD, 12));
		
		txtID = new JTextField();
		txtID.setEditable(false);
		txtID.setOpaque(false);
		txtID.setColumns(10);
		txtID.setBorder(new MatteBorder(0, 0, 1, 0, (Color) Color.BLACK));
		
		mbtnAdd = new MyButton();
		mbtnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnAddActionPerformed(e);
			}
		});
		mbtnAdd.setText("Add");
		mbtnAdd.setRadius(15);
		mbtnAdd.setColorOver(new Color(0, 191, 255));
		mbtnAdd.setColorClick(new Color(100, 149, 237));
		mbtnAdd.setBorderColor(SystemColor.textHighlight);
		mbtnAdd.setBorder(null);
		
		mbtnUpdate = new MyButton();
		mbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnUpdateActionPerformed(e);
			}
		});
		mbtnUpdate.setText("Update");
		mbtnUpdate.setRadius(15);
		mbtnUpdate.setColorOver(new Color(0, 191, 255));
		mbtnUpdate.setColorClick(new Color(100, 149, 237));
		mbtnUpdate.setBorderColor(SystemColor.textHighlight);
		mbtnUpdate.setBorder(null);
		
		mbtnDelete = new MyButton();
		mbtnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnDeleteActionPerformed(e);
			}
		});
		mbtnDelete.setText("Delete");
		mbtnDelete.setRadius(15);
		mbtnDelete.setColorOver(new Color(0, 191, 255));
		mbtnDelete.setColorClick(new Color(100, 149, 237));
		mbtnDelete.setBorderColor(SystemColor.textHighlight);
		mbtnDelete.setBorder(null);
		
		mbtnClear = new MyButton();
		mbtnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnClearActionPerformed(e);
			}
		});
		mbtnClear.setText("Clear");
		mbtnClear.setRadius(15);
		mbtnClear.setColorOver(new Color(0, 191, 255));
		mbtnClear.setColorClick(new Color(100, 149, 237));
		mbtnClear.setBorderColor(SystemColor.textHighlight);
		mbtnClear.setBorder(null);
		
		lblLevel = new JLabel("Level");
		lblLevel.setForeground(SystemColor.textHighlight);
		lblLevel.setFont(new Font("Arial", Font.BOLD, 12));
		
		comboBoxLevel = new JComboBox();
		comboBoxLevel.setModel(new DefaultComboBoxModel(new String[] {"------", "Admin", "Manager", "Seller"}));
		
		lblUsername = new JLabel("Username");
		lblUsername.setForeground(SystemColor.textHighlight);
		lblUsername.setFont(new Font("Arial", Font.BOLD, 12));
		
		txtUsername = new JTextField();
		txtUsername.setOpaque(false);
		txtUsername.setFont(new Font("SansSerif", Font.PLAIN, 13));
		txtUsername.setColumns(10);
		txtUsername.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		lblPassword = new JLabel("Password");
		lblPassword.setForeground(SystemColor.textHighlight);
		lblPassword.setFont(new Font("Arial", Font.BOLD, 12));
		
		txtpassword = new JTextField();
		txtpassword.setOpaque(false);
		txtpassword.setFont(new Font("SansSerif", Font.PLAIN, 13));
		txtpassword.setColumns(10);
		txtpassword.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		lblSearch = new JLabel("Search:");
		lblSearch.setForeground(SystemColor.textHighlight);
		lblSearch.setFont(new Font("SansSerif", Font.BOLD, 14));
		
		txtSearch = new JTextField();
		txtSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtSearchActionPerformed(e);
			}
		});
		txtSearch.setOpaque(false);
		txtSearch.setColumns(10);
		txtSearch.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(41)
					.addComponent(lblID, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
					.addGap(56)
					.addComponent(txtID, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(41)
					.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(9)
					.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(41)
					.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addGap(15)
					.addComponent(txtpassword, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(41)
					.addComponent(lblLevel, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
					.addGap(31)
					.addComponent(comboBoxLevel, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(50)
					.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(50)
					.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(50)
					.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(50)
					.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(lblSearch, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 896, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(4)
							.addComponent(lblID))
						.addComponent(txtID, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(17)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(4)
							.addComponent(lblUsername))
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(8)
							.addComponent(lblPassword))
						.addComponent(txtpassword, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(25)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(7)
							.addComponent(lblLevel))
						.addComponent(comboBoxLevel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(34)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(1)
							.addComponent(lblSearch))
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE))
		);
		setLayout(groupLayout);
		setVisible(true);
		LoadData();
	}
	private void LoadData() {
		AccountDao dao = new AccountDao();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Account ID");
		model.addColumn("username");
		model.addColumn("password");
		model.addColumn("level");
		
		dao.getListAccount().forEach(acc -> model.addRow(new Object[] {
				acc.getAccId(),acc.getUsername(),
				acc.getPassword(),acc.getLevel()== 1 ? "admin" 
								 :acc.getLevel() == 2 ? "manager":"seller"
		}));
		tableAccount.setModel(model);
	}
	
	private boolean validform() {
		if(Validation.Checkempty(txtUsername, "Have not entered Username!") == false) {
			JOptionPane.showMessageDialog(this, "Please enter Username");
			return false;
		}
		if(Validation.CheckRegex(txtUsername,RegexConst.USER, "Username is not correct format") == false) {
			return false;
		}
		if(Validation.Checkempty(txtpassword, "Have not entered Password!") == false) {
			JOptionPane.showMessageDialog(this, "Please enter Password");
			return false;
		}
		if(Validation.CheckRegex(txtpassword, RegexConst.PASSWORD, "Password must be 6 number") == false) {
			return false;
		}
		if(comboBoxLevel.getSelectedItem().toString().equals("------")) {
			JOptionPane.showMessageDialog(this, "fail");
			return false;
		}
		return true;
	}
	protected void mbtnAddActionPerformed(ActionEvent e) {
		AccountDao dao = new AccountDao();
		entity.Account acc = new entity.Account();
		if(validform() == false) {
			txtUsername.setText("");
			txtpassword.setText("");
			return;
		}
		acc.setUsername(txtUsername.getText());
		acc.setPassword(txtpassword.getText());
		acc.setLevel(comboBoxLevel.getSelectedItem().toString().equals("Admin")?1:
			comboBoxLevel.getSelectedItem().toString().equals("Manager")?2:0);
		if(dao.CheckExist(txtUsername.getText()) > 0) {
			JOptionPane.showMessageDialog(this, "Username is exist!!!!");
			return;
		}
		if(dao.AddAcc(acc) > 0) {
			LoadData();
			JOptionPane.showMessageDialog(this, "Success");
			refreshJFrame();
		}
		
	}
	protected void mbtnUpdateActionPerformed(ActionEvent e) {
		AccountDao dao = new AccountDao();
		entity.Account acc = new entity.Account();
		if(Validation.CheckRegex(txtID, RegexConst.NUMBER, "AccID must be number") == false) {
			return;
		}
		acc.setAccId(Integer.parseInt(txtID.getText()));
		if(validform() == false) {
			txtUsername.setText("");
			txtpassword.setText("");
			return;
		}
		acc.setUsername(txtUsername.getText());
		acc.setPassword(txtpassword.getText());
		acc.setLevel(comboBoxLevel.getSelectedItem().toString().equals("Admin")?1:
			comboBoxLevel.getSelectedItem().toString().equals("Manager")?2:0);
		dao.EditAcc(acc);
		LoadData();
		JOptionPane.showMessageDialog(this, "Update Success");
		refreshJFrame();
	}
	protected void mbtnDeleteActionPerformed(ActionEvent e) {
		AccountDao dao = new AccountDao();
		entity.Account acc = new entity.Account();
		if(txtUsername.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Nothing to delete");
		}else {
			acc.setAccId(Integer.parseInt(txtID.getText()));
			String str = txtID.getText();
			if(JOptionPane.showConfirmDialog(this, "Are you sure want to delete "+ str + "?","DELETE",
					JOptionPane.YES_NO_OPTION) == 0) {
				dao.DelAcc(acc);
				refreshJFrame();
				LoadData();
				JOptionPane.showMessageDialog(this, "Delete success");
			}
		}
		
	}
	
	protected void mbtnClearActionPerformed(ActionEvent e) {
		refreshJFrame();
		LoadData();
	}
	private void refreshJFrame() {
		txtUsername.setText("");
		txtpassword.setText("");
		txtID.setText("");
		mbtnAdd.setVisible(true);
	}
	protected void tableAccountMouseClicked(MouseEvent e) {
		mbtnAdd.setVisible(false);
		int rowIndex = tableAccount.getSelectedRow();
		txtID.setText(tableAccount.getValueAt(rowIndex, 0).toString());
		txtUsername.setText(tableAccount.getValueAt(rowIndex, 1).toString());
		txtpassword.setText(tableAccount.getValueAt(rowIndex, 2).toString());
		if(tableAccount.getValueAt(rowIndex, 3).toString().equals("admin")) {
			comboBoxLevel.setSelectedItem("Admin");
		}else if(tableAccount.getValueAt(rowIndex, 3).toString().equals("manager"))
		{comboBoxLevel.setSelectedItem("Manager");
		}else { comboBoxLevel.setSelectedItem("Seller");}
		
	}
	protected void txtSearchActionPerformed(ActionEvent e) {
		if(txtSearch.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Please enter account want to find");
		}else {
			String find = txtSearch.getText();
			DefaultRowSorter<?, ?> sorter = (DefaultRowSorter<?,?>) tableAccount.getRowSorter();
			sorter.setRowFilter(RowFilter.regexFilter(find));
			sorter.setSortKeys(null);
		}
	}
	
}
