package form;

import javax.swing.JPanel;
import javax.swing.DefaultRowSorter;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import extra.Table;
import component.MyButton;
import dao.AccountDao;
import dao.MedicineDao;
import dao.SaleDao;
import dao.StaffDao;
import form.Sales;
import gui.JframeLogin;
import form.Form_Invoice;
import helper.FormatNumber;
import entity.Invoice;
import entity.InvoiceDetail;
import entity.Medicine;
import entity.Staff;
import model.StatusType;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Sales extends JPanel {
	private JLabel lblQuantity;
	private JTextField txtQuantity;
	private JScrollPane scrollPaneInvoice;
	private Table tableInvoice;
	private MyButton mbtnAddToBill;
	private JLabel lblNewLabel;
	private JTextField txtSearch;
	private JScrollPane scrollPanePro;
	private Table tableProduct;
	private MyButton mbtnAddToBill_1;

	/**
	 * Create the panel.
	 */
	public Sales() {
		setBackground(Color.WHITE);
		setBounds(0,0,916,588);
		setVisible(true);
		
		lblQuantity = new JLabel("Quantity");
		lblQuantity.setFont(new Font("Arial", Font.BOLD, 12));
		lblQuantity.setForeground(SystemColor.textHighlight);
		
		txtQuantity = new JTextField();
		txtQuantity.setOpaque(false);
		txtQuantity.setColumns(10);
		txtQuantity.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		scrollPaneInvoice = new JScrollPane();
		
		tableInvoice = new Table();
		tableInvoice.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"STT", "ID","Name", "Unit", "Price", "Quality", "Total"
			}
		));
		
		scrollPaneInvoice.setViewportView(tableInvoice);
		
		mbtnAddToBill = new MyButton();
		mbtnAddToBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnAddToBillActionPerformed(e);
			}
		});
		mbtnAddToBill.setText("Add To Bill");
		mbtnAddToBill.setRadius(15);
		mbtnAddToBill.setColorOver(new Color(0, 191, 255));
		mbtnAddToBill.setColorClick(new Color(100, 149, 237));
		mbtnAddToBill.setBorderColor(SystemColor.textHighlight);
		mbtnAddToBill.setBorder(null);
		
		lblNewLabel = new JLabel("Search:");
		lblNewLabel.setForeground(SystemColor.textHighlight);
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 13));
		
		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				txtSearchKeyPressed(e);
			}
		});
		txtSearch.setOpaque(false);
		txtSearch.setColumns(10);
		txtSearch.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		scrollPanePro = new JScrollPane();
		
		tableProduct = new Table();
		tableProduct.setAutoCreateRowSorter(true);
		
		tableProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tableProductMouseClicked(e);
			}
		});
		
		scrollPanePro.setViewportView(tableProduct);
		
		mbtnAddToBill_1 = new MyButton();
		mbtnAddToBill_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnCreateActionPerformed(e);
			}
		});
		mbtnAddToBill_1.setText("Create");
		mbtnAddToBill_1.setRadius(15);
		mbtnAddToBill_1.setColorOver(new Color(0, 191, 255));
		mbtnAddToBill_1.setColorClick(new Color(100, 149, 237));
		mbtnAddToBill_1.setBorderColor(SystemColor.textHighlight);
		mbtnAddToBill_1.setBorder(null);
		
		mbtnClr = new MyButton();
		mbtnClr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnClrActionPerformed(e);
			}
		});
		mbtnClr.setText("Clear");
		mbtnClr.setRadius(15);
		mbtnClr.setColorOver(new Color(0, 191, 255));
		mbtnClr.setColorClick(new Color(100, 149, 237));
		mbtnClr.setBorderColor(SystemColor.textHighlight);
		mbtnClr.setBorder(null);
		
		lblInvoiceid = new JLabel("InvoiceId");
		lblInvoiceid.setForeground(SystemColor.textHighlight);
		lblInvoiceid.setFont(new Font("Arial", Font.BOLD, 12));
		
		txtInvoceId = new JTextField();
		txtInvoceId.setOpaque(false);
		txtInvoceId.setColumns(10);
		txtInvoceId.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		setLayout(null);
		
		dateChooserDate = new JDateChooser();
		dateChooserDate.setDate(new Date());
		dateChooserDate.setDateFormatString("dd-MM-yyyy");
		Date today = new Date();
		dateChooserDate.getDateEditor().addPropertyChangeListener(
			    new PropertyChangeListener() {
			        @Override
			        public void propertyChange(PropertyChangeEvent e) {
			            autoID();
			            if ("date".equals(e.getPropertyName())) {
			                if (((Date) e.getNewValue()).after(today)) {
			                	JOptionPane.showMessageDialog(dateChooserDate, "Date must be smaller or equals today!");
			                	dateChooserDate.setDate(today);
			                }
			            }
			        }
			    });
		this.add(dateChooserDate);
		lblDate = new JLabel("Date");
		lblDate.setForeground(SystemColor.textHighlight);
		lblDate.setFont(new Font("Arial", Font.BOLD, 12));
		
		lblNewLabel_1 = new JLabel("Total:");
		lblNewLabel_1.setFont(new Font("SansSerif", Font.BOLD, 16));
		lblNewLabel_1.setForeground(SystemColor.textHighlight);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblTotal = new JLabel("0,00");
		lblTotal.setFont(new Font("SansSerif", Font.BOLD, 14));
		
		mbtnRefresh = new MyButton();
		mbtnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnRefreshActionPerformed(e);
			}
		});
		mbtnRefresh.setText("Refresh");
		mbtnRefresh.setRadius(15);
		mbtnRefresh.setColorOver(new Color(0, 191, 255));
		mbtnRefresh.setColorClick(new Color(100, 149, 237));
		mbtnRefresh.setBorderColor(SystemColor.textHighlight);
		mbtnRefresh.setBorder(null);
		
		mbtnPrint = new MyButton();
		mbtnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mbtnPrintActionPerformed(e);
			}
		});
		mbtnPrint.setText("Print");
		mbtnPrint.setRadius(15);
		mbtnPrint.setColorOver(new Color(0, 191, 255));
		mbtnPrint.setColorClick(new Color(100, 149, 237));
		mbtnPrint.setBorderColor(SystemColor.textHighlight);
		mbtnPrint.setBorder(null);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 474, Short.MAX_VALUE)
					.addComponent(mbtnRefresh, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(78))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(scrollPanePro, GroupLayout.PREFERRED_SIZE, 872, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(lblInvoiceid, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(txtInvoceId, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
					.addGap(27)
					.addComponent(lblQuantity, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(txtQuantity, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDate, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(52)
							.addComponent(dateChooserDate, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)))
					.addGap(141)
					.addComponent(mbtnAddToBill, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(30)
					.addComponent(mbtnClr, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(scrollPaneInvoice, GroupLayout.PREFERRED_SIZE, 872, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(420)
					.addComponent(mbtnAddToBill_1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(mbtnPrint, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
					.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblTotal, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
					.addGap(27))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(8)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addComponent(mbtnRefresh, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addComponent(scrollPanePro, GroupLayout.PREFERRED_SIZE, 173, GroupLayout.PREFERRED_SIZE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(50)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(11)
									.addComponent(lblInvoiceid))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(11)
									.addComponent(lblQuantity))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(12)
									.addComponent(txtQuantity, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(8)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGap(4)
											.addComponent(lblDate))
										.addComponent(dateChooserDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addComponent(mbtnAddToBill, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnClr, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(62)
							.addComponent(txtInvoceId, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))
					.addGap(13)
					.addComponent(scrollPaneInvoice, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblTotal, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(29)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(mbtnPrint, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
								.addComponent(mbtnAddToBill_1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
							.addContainerGap())))
		);
		setLayout(groupLayout);
		loadMedicine();
		
	}
	
	private void loadMedicine () {
		MedicineDao dao = new MedicineDao();
		DefaultTableModel model = new DefaultTableModel();
				
		model.addColumn("Id");
		model.addColumn("Name");
		model.addColumn("Price");
		model.addColumn("Quantity");
		model.addColumn("Type");
		model.addColumn("Create date");
		
		
		dao.getListMedicine().forEach(med->model.addRow(new Object[] {
				med.getProID(),
				med.getName(),
				med.getPrice(),
				med.getQuantity(),
				med.getType(),
				med.getCreateDate()
		}));
		tableProduct.setModel(model);
	}
	
	protected void autoID() {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat date = new SimpleDateFormat("dd");
		DateFormat month = new SimpleDateFormat("MM");
		DateFormat year = new SimpleDateFormat("yyyy");
//		LocalDate date = LocalDate.ofInstant(dateChooserDate.getDate().toInstant(), ZoneId.systemDefault());
//		LocalDate date = dateChooserDate.getDate();
		SaleDao sale = new SaleDao();
		if (sale.getMaxID() == null) {
//			"ACT2207001"
			txtInvoceId.setText("ACT" + year.format(dateChooserDate.getDate()).substring(2) + month.format(dateChooserDate.getDate()) + "001");
		} else {
			Long id = Long.parseLong(sale.getMaxID().substring(7,sale.getMaxID().length()));
			id++;
			txtInvoceId.setText("ACT" + year.format(dateChooserDate.getDate()).substring(2) + month.format(dateChooserDate.getDate()) + String.format("%03d", id));
		}
	}
	private int proId;
	private String mediName;
	private double price;
	private String unit;
	private int quantity;
	protected void tableProductMouseClicked(MouseEvent e) {
		int rowIndex = tableProduct.getSelectedRow();
		proId = Integer.parseInt(tableProduct.getValueAt(rowIndex, 0).toString());
		mediName = tableProduct.getValueAt(rowIndex, 1).toString();
		price = Double.parseDouble(tableProduct.getValueAt(rowIndex, 2).toString());
		unit = tableProduct.getValueAt(rowIndex, 4).toString();
		quantity = Integer.parseInt(tableProduct.getValueAt(rowIndex, 3).toString());
		
	}
	private int i = 1;
	private MyButton mbtnClr;
	private JLabel lblInvoiceid;
	private JTextField txtInvoceId;
	private JDateChooser dateChooserDate;
	private JLabel lblDate;
	private JLabel lblNewLabel_1;
	private JLabel lblTotal;
	private MyButton mbtnRefresh;
	private MyButton mbtnPrint;
	protected void mbtnAddToBillActionPerformed(ActionEvent e) {
		Medicine med = new Medicine();
		MedicineDao medDao = new MedicineDao();
		double sum = 0;
		int totalQuantity = 0;
		if (txtQuantity.getText().isEmpty()) {
			totalQuantity = 0;
		} else {
			totalQuantity = Integer.parseInt(txtQuantity.getText());
		}
		double total = (double)(price * totalQuantity);
		FormatNumber fn = new FormatNumber();
		DefaultTableModel df = (DefaultTableModel)tableInvoice.getModel();
		if (proId == 0 && mediName == null && unit == null) {
			JOptionPane.showMessageDialog(tableProduct, "Please choose product!");
			return;
		}
		if (txtQuantity.getText().isEmpty()) {
			JOptionPane.showMessageDialog(txtQuantity, "Please enter quantity!");
			return;
		} else if (medDao.selectQuantityById(proId) <= 0) {
			JOptionPane.showMessageDialog(txtQuantity, "Product is out of stock !");
			return;
		} else if (medDao.selectQuantityById(proId) < Integer.valueOf(txtQuantity.getText())) {
			JOptionPane.showMessageDialog(txtQuantity, "Quantity is lager than Stock !");
			return;
		} else {
			for (int j=0;j<df.getRowCount();j++) {
				if (Integer.parseInt(df.getValueAt(j, 1).toString()) == proId) {
					totalQuantity = Integer.parseInt(txtQuantity.getText()) + Integer.parseInt(df.getValueAt(j, 5).toString());
					df.setValueAt(totalQuantity, j, 5);
					df.setValueAt(fn.DoubleToString((double)(price * totalQuantity)), j, 6);
					for (int k = 0;k<df.getRowCount();k++) {
						double amount = Double.parseDouble(fn.Replace(tableInvoice.getValueAt(k, 6).toString()));
						sum += amount;
						lblTotal.setText(fn.DoubleToString(sum));
					}
					return;
				} else {
						i++;
						totalQuantity = Integer.parseInt(txtQuantity.getText());
				}
			}
			df.addRow(new Object[]
			{
				i,
				proId,
				mediName,
				unit,
				fn.DoubleToString(price),
				totalQuantity,
				fn.DoubleToString(total)
			});
			for (int l = 0;l<df.getRowCount();l++) {
				double amount = Double.parseDouble(fn.Replace(tableInvoice.getValueAt(l, 6).toString()));
				sum += amount;
			}
			lblTotal.setText(fn.DoubleToString(sum));
		}
	}
	
	protected void mbtnClrActionPerformed(ActionEvent e) {
		refreshTable();
	}
	
	protected void refreshTable() {
		DefaultTableModel df = (DefaultTableModel)tableInvoice.getModel();
		df.setRowCount(0);
		lblTotal.setText("0,00");
		i = 0;
	}
	protected void mbtnCreateActionPerformed(ActionEvent e) {
		Form_Invoice invF = new Form_Invoice();
		FormatNumber fn = new FormatNumber();
		DefaultTableModel df = (DefaultTableModel)tableInvoice.getModel();
		DefaultTableModel product = (DefaultTableModel)tableProduct.getModel();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		SaleDao sale = new SaleDao();
		MedicineDao medDao = new MedicineDao();
		AccountDao acc = new AccountDao();
		Invoice inv = new Invoice();
		Medicine med = new Medicine();
		StaffDao staffDao = new StaffDao();
		InvoiceDetail ind = new InvoiceDetail();
		if (sale.selectInvoiceById(txtInvoceId.getText().toString()) > 0) {
			JOptionPane.showMessageDialog(txtInvoceId, "This invoice ID is existed!");
			return;
		} else {
			inv.setInvoiceId(txtInvoceId.getText());
		}
		
		inv.setDateOfCreate(LocalDate.ofInstant(dateChooserDate.getDate().toInstant(), ZoneId.systemDefault()));
		int accId = acc.showAccId(JframeLogin.username);
		inv.setStaffId(staffDao.selectStaffByAccid(accId));
		if (sale.insertInvoice(inv) > 0 ) {
			int count = 0;
			for(int i = 0; i < df.getRowCount(); i++ ) {
				ind.setInvoiceId(txtInvoceId.getText());
				ind.setProId(Integer.valueOf(df.getValueAt(i, 1).toString()));
				ind.setQuantity(Integer.valueOf(df.getValueAt(i, 5).toString()));
				ind.setTotal(Double.parseDouble(fn.Replace(df.getValueAt(i, 6).toString())));
				med.setProID(Integer.valueOf(df.getValueAt(i, 1).toString()));
				int quantity = medDao.selectQuantityById(med.getProID()) - Integer.valueOf(df.getValueAt(i, 5).toString());
				med.setQuantity(quantity);
				if (sale.insertInvoiceDetail(ind) > 0) {
					count++;
					medDao.updateMedicineAtferInvoice(med);
				} else {
					JOptionPane.showMessageDialog(this, "Failed!");
				}
			}
			JOptionPane.showMessageDialog(this, "Success!");
		} else {
			JOptionPane.showMessageDialog(null, "Failed!");
		}	
		refreshTable();
		autoID();
		Home.lblValue2.setText(fn.DoubleToString(sale.sumProfit()));
		
	}
	protected void mbtnRefreshActionPerformed(ActionEvent e) {
		loadMedicine();
	}
	protected void txtSearchKeyPressed(KeyEvent e) {
		String find = txtSearch.getText();
		DefaultRowSorter<?, ?> sorter = 
				(DefaultRowSorter<?, ?>) tableProduct.getRowSorter();
		sorter.setRowFilter(RowFilter.regexFilter(find));
		sorter.setSortKeys(null);
	}
	protected void mbtnPrintActionPerformed(ActionEvent e) {
//		MessageFormat header = new MessageFormat("ACT PHARMACY");
//		MessageFormat footer = new MessageFormat("THANK YOU");
//		try {
//			tableInvoice.print(JTable.PrintMode.FIT_WIDTH, header, footer);
//		} catch (Exception e2) {
//			JOptionPane.showMessageDialog(null, "Cannot be print !");
//		}
		InvoiceReport invoice = new InvoiceReport();
		invoice.setVisible(true);
	}
}
