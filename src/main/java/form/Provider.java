package form;

import javax.swing.JPanel;
import javax.swing.DefaultRowSorter;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import extra.Table;
import helper.RegexConst;
import helper.Validation;
import component.MyButton;
import dao.CategoryDao;
import dao.ProviderDao;
import entity.Categories;
import entity.Providers;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class Provider extends JPanel {
	private JLabel lblIdUser;
	private JLabel lblName;
	private JTextField txtId;
	private JLabel lblPhone;
	private JLabel lblEmail;
	private JTextField txtPhone;
	private JTextField txtName;
	private JTextField txtEmail;
	private JLabel lblAddress;
	private MyButton mbtnClear;
	private MyButton mbtnDelete;
	private MyButton mbtnUpdate;
	private MyButton mbtnAdd;
	private JScrollPane scrollPane;
	private Table table;
	private JTextField txtSearch;
	private JLabel lblNewLabel;
	private JLabel label;
	private JTextArea textArea;

	/**
	 * Create the panel.
	 */
	public Provider() {
		setBackground(Color.WHITE);
		setBounds(0,0,916,588);
		setVisible(true);
		
		lblIdUser = new JLabel("ID");
		lblIdUser.setBounds(336, 44, 23, 14);
		lblIdUser.setFont(new Font("Arial", Font.BOLD, 12));
		lblIdUser.setForeground(SystemColor.textHighlight);
		
		lblName = new JLabel("Name");
		lblName.setBounds(336, 77, 64, 14);
		lblName.setFont(new Font("Arial", Font.BOLD, 12));
		lblName.setForeground(SystemColor.textHighlight);
		
		txtId = new JTextField();
		txtId.setEditable(false);
		txtId.setBounds(410, 43, 80, 15);
		txtId.setOpaque(false);
		txtId.setColumns(10);
		txtId.setBorder(new MatteBorder(0, 0, 1, 0, (Color) Color.BLACK));
		
		lblPhone = new JLabel("Phone");
		lblPhone.setBounds(543, 47, 55, 14);
		lblPhone.setFont(new Font("Arial", Font.BOLD, 12));
		lblPhone.setForeground(SystemColor.textHighlight);
		
		lblEmail = new JLabel("Email");
		lblEmail.setBounds(543, 80, 64, 14);
		lblEmail.setFont(new Font("Arial", Font.BOLD, 12));
		lblEmail.setForeground(SystemColor.textHighlight);
		
		txtPhone = new JTextField();
		txtPhone.setBounds(611, 47, 80, 15);
		txtPhone.setOpaque(false);
		txtPhone.setColumns(10);
		txtPhone.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		txtName = new JTextField();
		txtName.setBounds(410, 74, 80, 15);
		txtName.setOpaque(false);
		txtName.setColumns(10);
		txtName.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		txtEmail = new JTextField();
		txtEmail.setBounds(611, 74, 80, 15);
		txtEmail.setOpaque(false);
		txtEmail.setColumns(10);
		txtEmail.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		lblAddress = new JLabel("Address");
		lblAddress.setBounds(540, 50, 64, 14);
		lblAddress.setFont(new Font("Arial", Font.BOLD, 12));
		lblAddress.setForeground(SystemColor.textHighlight);
		
		mbtnClear = new MyButton();
		mbtnClear.setBounds(435, 187, 70, 30);
		mbtnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnClear_actionPerformed(e);
			}
		});
		mbtnClear.setText("Clear");
		mbtnClear.setRadius(15);
		mbtnClear.setColorOver(new Color(0, 191, 255));
		mbtnClear.setColorClick(new Color(100, 149, 237));
		mbtnClear.setBorderColor(SystemColor.textHighlight);
		mbtnClear.setBorder(null);
		
		mbtnDelete = new MyButton();
		mbtnDelete.setBounds(320, 187, 70, 30);
		mbtnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnDelete_actionPerformed(e);
			}
		});
		mbtnDelete.setText("Delete");
		mbtnDelete.setRadius(15);
		mbtnDelete.setColorOver(new Color(0, 191, 255));
		mbtnDelete.setColorClick(new Color(100, 149, 237));
		mbtnDelete.setBorderColor(SystemColor.textHighlight);
		mbtnDelete.setBorder(null);
		
		mbtnUpdate = new MyButton();
		mbtnUpdate.setBounds(207, 187, 70, 30);
		mbtnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnUpdate_actionPerformed(e);
			}
		});
		mbtnUpdate.setText("Update");
		mbtnUpdate.setRadius(15);
		mbtnUpdate.setColorOver(new Color(0, 191, 255));
		mbtnUpdate.setColorClick(new Color(100, 149, 237));
		mbtnUpdate.setBorderColor(SystemColor.textHighlight);
		mbtnUpdate.setBorder(null);
		
		mbtnAdd = new MyButton();
		mbtnAdd.setBounds(86, 187, 70, 30);
		mbtnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_mbtnAdd_actionPerformed(e);
			}
		});
		mbtnAdd.setText("Add");
		mbtnAdd.setRadius(15);
		mbtnAdd.setColorOver(new Color(0, 191, 255));
		mbtnAdd.setColorClick(new Color(100, 149, 237));
		mbtnAdd.setBorderColor(SystemColor.textHighlight);
		mbtnAdd.setBorder(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 294, 896, 284);
		
		table = new Table();
		ProviderDao dao = new ProviderDao();
		loadProvider(dao);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tableMouseClicked(e);
			}
		});
		scrollPane.setViewportView(table);
		
		txtSearch = new JTextField();
		txtSearch.setBounds(68, 254, 286, 30);
		txtSearch.setOpaque(false);
		txtSearch.setColumns(10);
		txtSearch.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		lblNewLabel = new JLabel("Search:");
		lblNewLabel.setBounds(10, 262, 69, 22);
		lblNewLabel.setForeground(SystemColor.textHighlight);
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 13));
		
		label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		setLayout(null);
		
		textArea = new JTextArea();
		textArea.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(108)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblName)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(txtName, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(lblEmail, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblIdUser, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(txtId, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(lblPhone, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(txtEmail)
								.addComponent(txtPhone, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE))
							.addGap(34)
							.addComponent(lblAddress, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(64)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(44)
									.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(51)
									.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(43)
									.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(45)
									.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblNewLabel)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 286, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(39)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 835, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(42, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(53)
									.addComponent(lblPhone))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(53)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(txtPhone, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblAddress)))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(49)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGap(1)
											.addComponent(lblIdUser, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
										.addComponent(txtId, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(15)
									.addComponent(lblName))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(12)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtName, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(18)
									.addComponent(lblEmail))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(39)
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)))
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(mbtnAdd, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnUpdate, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnDelete, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
						.addComponent(mbtnClear, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(37)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
							.addGap(8)))
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 282, GroupLayout.PREFERRED_SIZE)
					.addGap(30))
		);
		setLayout(groupLayout);

	}
	protected void tableMouseClicked (MouseEvent e) {
		int rowIndex = table.getSelectedRow();
		ProviderDao dao = new ProviderDao();
		
		txtId.setText(table.getValueAt(rowIndex, 0).toString());
		var id = table.getValueAt(rowIndex, 0).toString();
		
		dao.getListProvider().forEach(x-> {if(x.getProviderID() == Integer.parseInt(id)) {
			txtId.setText(String.valueOf(x.getProviderID()));
			txtName.setText(x.getName());
			txtPhone.setText(x.getPhone());
			txtEmail.setText(x.getEmail());
			textArea.setText(x.getAddress());
		}});
		mbtnAdd.setVisible(false);
	}
	private boolean validform() {
		if(Validation.Checkempty(txtName, "Have not entered provider name!") == false) {
			return false;
		}
		if(Validation.Checkempty(txtPhone, "Have not entered Phone!") == false) {
			return false;
		}
		if(Validation.CheckRegex(txtPhone, RegexConst.NUMBER, "Phone not valid") == false) {
			return false;
		}
		if(Validation.Checkempty(txtEmail, "Have not entered email!") == false) {
			return false;
		}
		if(Validation.Checkempty(textArea, "Have not entered address") == false) {
			return false;
		}
		if(Validation.CheckRegex(txtEmail, RegexConst.EMAIL, "Email not valid") == false) {
			return false;
		}
		String address = textArea.getText().trim();
		if(address.equals("")) {
			JOptionPane.showMessageDialog(this, "Please enter Address", "Inane error",
				    JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	private void loadProvider(ProviderDao dao) {
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("Id");
		model.addColumn("Name");
		model.addColumn("Phone");
		model.addColumn("Email");
		model.addColumn("Address");
		dao.getListProvider().forEach(pro->model.addRow(new Object[] {
				pro.getProviderID(),
				pro.getName(),
				pro.getPhone(),
				pro.getEmail(),
				pro.getAddress()
		}));
		table.setModel(model);
	}
	private void clearText() {
		txtId.setText("");
		txtName.setText("");
		txtPhone.setText("");
		txtEmail.setText("");
		textArea.setText("");
		mbtnAdd.setVisible(true);
	}
	protected void do_txtSearch_keyPressed(KeyEvent e) {
		String find = txtSearch.getText();
		DefaultRowSorter<?, ?> sorter = 
				(DefaultRowSorter<?, ?>) table.getRowSorter();
		sorter.setRowFilter(RowFilter.regexFilter(find));
		sorter.setSortKeys(null);
	}
	protected void do_mbtnAdd_actionPerformed(ActionEvent e) {
		if(validform() == false) {
			return;
		}
		ProviderDao dao = new ProviderDao();
		Providers pro = new Providers();
		if (dao.checkNameExist(txtName.getText()).isEmpty()) {
			pro.setName(txtName.getText());
			pro.setPhone(txtPhone.getText());
			pro.setEmail(txtEmail.getText());
			pro.setAddress(textArea.getText());
			
			dao.insertProvider(pro);
			loadProvider(dao);	
			JOptionPane.showMessageDialog(this, "Add successfully!");
			clearText();
		}else {
			
			JOptionPane.showMessageDialog(this, "Provider is exist");
			
		}
		
	}
	protected void do_mbtnUpdate_actionPerformed(ActionEvent e) {
		if(txtId.getText().equals("")){
			JOptionPane.showMessageDialog(this, "Please choose provider you want to update!");
			return;
		}
		if(validform() == false) {
			return;
		}
		ProviderDao dao = new ProviderDao();
		Providers pro = new Providers();
		
		pro.setName(txtName.getText());
		pro.setPhone(txtPhone.getText());
		pro.setEmail(txtEmail.getText());
		pro.setAddress(textArea.getText());
		pro.setProviderID(Integer.parseInt(txtId.getText()));
		
		dao.updateProvider(pro);
		loadProvider(dao);		
		JOptionPane.showMessageDialog(this, "Update successfully!");
		clearText();
	}
	protected void do_mbtnDelete_actionPerformed(ActionEvent e) {
		ProviderDao dao = new ProviderDao();
		if(txtId.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Nothing to delete");
			return;
		} 
		if(JOptionPane.showConfirmDialog(this, "Are you sure want to delete?", "DELETE",
				JOptionPane.YES_NO_OPTION) == 0) {
			int rowIndex = table.getSelectedRow();
			txtId.setText(table.getValueAt(rowIndex, 0).toString());
			int id = (int) table.getValueAt(rowIndex, 0);
			
			dao.deleteProvider(id);
			JOptionPane.showMessageDialog(this, "Delete successfully");
			loadProvider(dao);	
			clearText();
		}
	}
	protected void do_mbtnClear_actionPerformed(ActionEvent e) {
		clearText();
	}
}
