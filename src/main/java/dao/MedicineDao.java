package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Date;
import java.util.*;

import common.ConnectDB;
import entity.Medicine;

public class MedicineDao {
	
	public 	List<Medicine> getListMedicine(){
		List<Medicine> list = new ArrayList<Medicine>();
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("select * from Product");
				ResultSet rs = cs.executeQuery();		
				)
			{
				while(rs.next()) {
					Medicine med = new Medicine();
					med.setProID(rs.getInt("proID"));
					med.setName(rs.getString("name"));
					med.setCateID(rs.getInt("cateID"));
					med.setDecription(rs.getString("decription"));
					med.setPrice(rs.getFloat("price"));
					med.setProvider(rs.getInt("providerID"));
					med.setDateOfManufacture(rs.getDate("dateOfManufacture").toLocalDate());
					med.setExpiryDate(rs.getDate("expiryDate").toLocalDate());
					med.setPicture(rs.getString("picture"));
					med.setIngredients(rs.getString("ingredients"));
					med.setUses(rs.getString("uses"));
					med.setBrand(rs.getString("brand"));
					med.setType(rs.getString("type"));
					med.setQuantity(rs.getInt("quantity"));
					med.setCreateDate(rs.getDate("createDate").toLocalDate());
					
					list.add(med);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return list;
	}
	
	public void insertMedicine(Medicine med) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("insert into Product (name, cateID, decription, price, providerID, dateOfManufacture,expiryDate, ingredients, uses, brand, type , quantity, picture, createDate) "
							+ "	values (?, ?, ?,?, ?, ?,?, ?, ?,?, ?,?,?,?) ");	
			)
		{
			cs.setString(1, med.getName());
			cs.setInt(2, med.getCateID());
			cs.setString(3, med.getDecription());
			cs.setFloat(4, med.getPrice());
			cs.setInt(5, med.getProvider());
			cs.setDate(6, Date.valueOf(med.getDateOfManufacture()));
			cs.setDate(7, Date.valueOf(med.getExpiryDate()));
			cs.setString(8, med.getIngredients());
			cs.setString(9, med.getUses());
			cs.setString(10, med.getBrand());
			cs.setString(11, med.getType());
			cs.setInt(12, med.getQuantity());
			cs.setString(13, med.getPicture());
			cs.setDate(14, Date.valueOf(med.getCreateDate()));
			
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateMedicine(Medicine med) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("UPDATE Product "
								+ "SET name = ?, cateID = ?, decription = ? , price = ?, providerID = ?, dateOfManufacture = ?, expiryDate = ?, ingredients = ?, uses = ?, brand = ?, type =?, quantity = ?, picture =? "
								+ "WHERE proID = ? ");	
			)
		{
			cs.setString(1, med.getName());
			cs.setInt(2, med.getCateID());
			cs.setString(3, med.getDecription());
			cs.setFloat(4, med.getPrice());
			cs.setInt(5, med.getProvider());
			cs.setDate(6, Date.valueOf(med.getDateOfManufacture()));
			cs.setDate(7, Date.valueOf(med.getExpiryDate()));
			cs.setString(8, med.getIngredients());
			cs.setString(9, med.getUses());
			cs.setString(10, med.getBrand());
			cs.setString(11, med.getType());
			cs.setInt(12, med.getQuantity());
			cs.setString(13, med.getPicture());
			cs.setInt(14, med.getProID());
			
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void deleteMedicine(int id) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("delete from Product where proID = " + id);	
			)
		{
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateMedicineAtferInvoice(Medicine med) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("UPDATE Product "
								+ " SET quantity = ?"
								+ " WHERE proID = ? ");	
			)
		{
			
			cs.setInt(1, med.getQuantity());
			cs.setInt(2, med.getProID());
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int selectQuantityById(int proId) {
		int quantity = 0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call selquantityById("+ proId +")}");
				ResultSet rs = cs.executeQuery();
				)
			{
				while(rs.next()) {
					int number = rs.getInt("quantity");
					quantity = number;
				}
			} catch (Exception e) {
				e.printStackTrace();
//				System.out.println(e.getMessage());
			}
		return quantity;
	}
	
	public int sumProduct() {
		int total = 0;
		try 
		(
			Connection connect = ConnectDB.getConnect();
			Statement s = connect.createStatement();
			ResultSet rs = s.executeQuery("select sum(quantity) as quantity from Product");
		)
		{
			rs.next();
			total = rs.getInt("quantity");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return total;
	}
}
