package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import common.ConnectDB;
import entity.Categories;
import entity.Providers;

public class ProviderDao {
	public 	List<Providers> getListProvider(){
		List<Providers> list = new ArrayList<Providers>();
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("SELECT * FROM Providers");
			ResultSet rs = cs.executeQuery();		
			)
		{
			while(rs.next()) {
				Providers pro = new Providers();
				
				pro.setProviderID(rs.getInt("providerID"));
				pro.setName(rs.getString("name"));
				pro.setPhone(rs.getString("Phone"));
				pro.setEmail(rs.getString("email"));
				pro.setAddress(rs.getString("address"));
				
				list.add(pro);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	} 
	public void insertProvider(Providers pro) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("insert into Providers (name, phone, email, address) values (?, ?, ?, ?) ");	
				
			)
		{
			cs.setString(1, pro.getName());
			cs.setString(2, pro.getPhone());
			cs.setString(3, pro.getEmail());
			cs.setString(4, pro.getAddress());
			
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateProvider(Providers pro) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("UPDATE Providers SET name = ?, phone = ?, email = ?, address = ? WHERE providerID = ? ");	
			)
		{
			cs.setString(1, pro.getName());
			cs.setString(2, pro.getPhone());
			cs.setString(3, pro.getEmail());
			cs.setString(4, pro.getAddress());
			cs.setInt(5, pro.getProviderID());
			
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void deleteProvider(int id) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("delete from Providers where providerID = " + id);	
			)
		{
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public 	List<Providers> checkNameExist(String name){
		List<Providers> list = new ArrayList<Providers>();
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("select name from providers where name = '" + name+"'");
			ResultSet rs = cs.executeQuery();		
			)
		{
			Providers pro = new Providers();
			while(rs.next()) {
				pro.setName(rs.getString("name"));
				
				list.add(pro);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	} 
}
