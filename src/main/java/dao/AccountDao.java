package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import common.ConnectDB;
import entity.Account;

public class AccountDao {
	public List<Account> getListAccount() {
//		var listN = new ArrayList<Employee>();
		List<Account> list = new ArrayList<Account>();

		try(
			Connection con = ConnectDB.getConnect();
			CallableStatement cs = con.prepareCall("{call selAcc}");
			ResultSet rs = cs.executeQuery();			
			) {
			while(rs.next()) {
				Account acc = new Account();
				acc.setAccId(rs.getInt("AccId"));
				acc.setUsername(rs.getNString("username"));
				acc.setPassword(rs.getNString("password"));
				acc.setLevel(rs.getInt("level"));
				list.add(acc);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return list;
	}
	public int AddAcc(Account acc) {
		int i = 0;
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call AddAcc(?,?,?) }");
		){
			cs.setString(1, acc.getUsername());
			cs.setString(2, acc.getPassword());
			cs.setInt(3, acc.getLevel());
			i = cs.executeUpdate();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return i;
	}
	public void EditAcc(Account acc) {
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call EditAcc(?,?,?,?) }");
		){
			cs.setInt(1, acc.getAccId());
			cs.setString(2, acc.getUsername());
			cs.setString(3, acc.getPassword());
			cs.setInt(4, acc.getLevel());
			cs.executeUpdate();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	public void DelAcc(Account acc) {
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call DelAcc(?) }");
		){
			cs.setInt(1, acc.getAccId());
			cs.executeUpdate();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	private static CallableStatement createCs(Connection con,
			String username, String password) throws SQLException {
		String sql = "{call login(?,?)}";
		CallableStatement cs = con.prepareCall(sql);
		cs.setString(1, username);
		cs.setString(2, password);
		return cs;
	}
	public int login(String username, String password) {
		int count=0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = createCs(connect, username, password);
				ResultSet rs = cs.executeQuery();			
			) {
				while(rs.next()) {
					count =rs.getInt(1);
				}
		} catch (Exception e) {
			e.getMessage(); 
		}
		return count;
	}
	private static CallableStatement showU(String sql, Connection con,
			String username, String password) throws SQLException {
//		String sql = "{call showuser(?,?)}";
		CallableStatement cs = con.prepareCall(sql);
		cs.setString(1, username);
		cs.setString(2, password);
		return cs;
	}
	public String showuser(String username, String password) {
		String name = null;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = showU("{call showuser(?,?)}",connect, username, password);
				ResultSet rs = cs.executeQuery();
			){
				while(rs.next()) {
					name = rs.getNString("username");
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		return name;
	}
	private static CallableStatement CreateCS(String sql ,Connection con,
			String username) throws SQLException {
		CallableStatement cs = con.prepareCall(sql);
		cs.setString(1, username);

		return cs;
	}
	public int showAccId(String username) {
		int accid = 0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = CreateCS("{call showAccId(?)}",connect,username);
				ResultSet rs = cs.executeQuery();
			){
				while(rs.next()) {
					accid = rs.getInt("AccId");
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		return accid;
	}
	public List<Account> showUsername() {
		List<Account> list = new ArrayList<Account>();
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call showAll}");
			ResultSet rs = cs.executeQuery();
		){
			while(rs.next()) {
				Account acc = new Account();
				acc.setUsername(rs.getNString("username"));
				list.add(acc);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return list;
	}
	public int CheckExist (String username) {
		int count = 0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = CreateCS("{call checkExist(?)}",connect,username);
				ResultSet rs = cs.executeQuery();
			){
				while(rs.next()) {
					count = count =rs.getInt(1);
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		return count;
	}
	
	public int level (String username, String password) {
		int level =0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call level("+username+","+password+")}");
				ResultSet rs = cs.executeQuery();
			){
				while(rs.next()) {
					level = rs.getInt("level");
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		return level;
	}
	
	public String fullname (String username, String password) {
		String fullname = null;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call fullname("+username+","+password+")}");
				ResultSet rs = cs.executeQuery();
			){
				while(rs.next()) {
					fullname = rs.getNString("fullname");
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		return fullname;
	}
	
	public String getusername (int id) {
		String username = null;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call getuser("+id+")}");
				ResultSet rs = cs.executeQuery();
			){
				while(rs.next()) {
					username = rs.getNString("username");
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		return username;
	}
	
	
		
}
