package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import common.ConnectDB;
import entity.Categories;

public class CategoryDao {
	public 	List<Categories> getListCategory(){
		List<Categories> list = new ArrayList<Categories>();
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("SELECT * FROM Category");
			ResultSet rs = cs.executeQuery();		
			)
		{
			while(rs.next()) {
				Categories cate = new Categories();
				cate.setCateID(rs.getInt("cateID"));
				cate.setName(rs.getString("name"));
				cate.setStatus(rs.getBoolean("status"));
				
				list.add(cate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	} 
	public void insertCategory(Categories cate) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("insert into Category (name, status) "
							+ "	values (?, ?) ");	
				
			)
		{
			cs.setString(1, cate.getName());
			cs.setString(2, Boolean.toString(cate.isStatus()));
			
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateMedicine(Categories cate) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("UPDATE Category "
								+ "SET name = ?, status = ? "
								+ "WHERE cateID = ? ");	
			)
		{
			cs.setString(1, cate.getName());
			cs.setBoolean(2, cate.isStatus());
			cs.setInt(3, cate.getCateID());
			
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void deleteCategory(int id) {
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect
						.prepareCall("delete from Category where cateID = " + id);	
			)
		{
			cs.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public 	List<Categories> checkNameExist(String name){
		List<Categories> list = new ArrayList<Categories>();
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("select name from category where name = '" + name+"'");
			ResultSet rs = cs.executeQuery();		
			)
		{
			Categories cate = new Categories();
			while(rs.next()) {
				cate.setName(rs.getString("name"));
				
				list.add(cate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	} 
	public 	List<Categories> getListCateInUse(Categories cate){
		List<Categories> list = new ArrayList<Categories>();
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("select name from category where status = 1");
			ResultSet rs = cs.executeQuery();		
			)
		{
			while(rs.next()) {
				cate.setName(rs.getString("name"));
				
				list.add(cate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	} 
}
