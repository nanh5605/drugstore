package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import common.ConnectDB;
import entity.Invoice;
import entity.InvoiceDetail;
import entity.Medicine;
import entity.Staff;

public class SaleDao {
	public List<Invoice> getListInvoice() {
		List<Invoice> list = new ArrayList<Invoice>();
		try(
				Connection connect = ConnectDB.getConnect();
				Statement s = connect.createStatement();
				ResultSet rs = s.executeQuery("select Invoice.invoiceID, Invoice.dateOfCreation, SUM(total) as sumTotal, Invoice.staffId\r\n"
						+ "from InvoiceDetail\r\n"
						+ "inner Join Invoice ON Invoice.invoiceID = InvoiceDetail.invoiceID\r\n"
						+ "group by Invoice.invoiceID, Invoice.dateOfCreation, Invoice.staffId");
			)
		{
			while(rs.next()) {
				Invoice inv = new Invoice();
				inv.setInvoiceId(rs.getString("invoiceID"));
				inv.setDateOfCreate(rs.getDate("dateOfCreation").toLocalDate());
				inv.setTotal(rs.getDouble("sumTotal"));
				inv.setStaffId(rs.getInt("staffId"));
				list.add(inv);
			}
		} catch (Exception e) {
			e.printStackTrace();
//			System.out.println(e.getMessage());
		}
		return list;
	}
	public int insertInvoice(Invoice inv) {
		int i = 0;
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call insInvoice(?,?,?)}");	
			)
		{
			cs.setString(1, inv.getInvoiceId());
			cs.setDate(2, Date.valueOf(inv.getDateOfCreate()));
			cs.setInt(3, inv.getStaffId());
			i = cs.executeUpdate();
		} catch (Exception e) {
//			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return i;
	}
	
	public int insertInvoiceDetail(InvoiceDetail ind) {
		int i = 0;
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call insInvoiceDetail(?,?,?,?)}");	
			)
		{

			cs.setString(1, ind.getInvoiceId());
			cs.setInt(2, ind.getProId());
			cs.setInt(3, ind.getQuantity());
			cs.setDouble(4, ind.getTotal());
			i = cs.executeUpdate();
		} catch (Exception e) {
//			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return i;
	}
	
	public String selectStaffById(int id) {
		String staffName = "";
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call selStaffnameById("+ id +")}");
				ResultSet rs = cs.executeQuery();
				)
			{
				while(rs.next()) {
					String name = rs.getString("fullname");
					staffName = name;
				}
			} catch (Exception e) {
				e.printStackTrace();
//				System.out.println(e.getMessage());
			}
		return staffName;
	}
	public int selectInvoiceById(String invoiceId) {
		int count = 0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call selInvoiceById("+ invoiceId +")}");
				ResultSet rs = cs.executeQuery();
				)
			{
				while(rs.next()) {
					int id = rs.getInt("countInvId");
					count = id;
				}
			} catch (Exception e) {
				e.printStackTrace();
//				System.out.println(e.getMessage());
			}
		return count;
	}
	
	public List<InvoiceDetail> getListInvoiceDetail(String invoiceID) {
		List<InvoiceDetail> list = new ArrayList<InvoiceDetail>();
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call selInvoiceDetal("+ invoiceID +")}");
			ResultSet rs = cs.executeQuery();
			)
		{
			while(rs.next()) {
				InvoiceDetail ind = new InvoiceDetail();
//				inv.setInvoiceId(rs.getString("invoiceID"));
//				inv.setDateOfCreate(rs.getDate("dateOfCreation").toLocalDate());
//				inv.setStaffId(rs.getInt("staffId"));
//				list.add(inv);
				ind.setProName(rs.getString("name"));
				ind.setUnit(rs.getString("type"));
				ind.setPrice(rs.getDouble("price"));
				ind.setQuantity(rs.getInt("quantity"));
				list.add(ind);
			}
		} catch (Exception e) {
			e.printStackTrace();
//			System.out.println(e.getMessage());
		}
		return list;
	}
	
	public String getMaxID() {
		String maxID = null;
		try 
		(
			Connection connect = ConnectDB.getConnect();
			Statement s = connect.createStatement();
			ResultSet rs = s.executeQuery("select Max(invoiceID) as maxID from Invoice");
		)
		{
			rs.next();
			maxID = rs.getString("maxID");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return maxID;
	}
	
	public int deleteInvoice(Invoice inv) {
		int i = 0;
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call delInvoice(?)}");		
			)
		{
			cs.setString(1, inv.getInvoiceId());
			i = cs.executeUpdate();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return i;
	}
	
	public double sumProfit() {
		double profit = 0;
		try 
		(
			Connection connect = ConnectDB.getConnect();
			Statement s = connect.createStatement();
			ResultSet rs = s.executeQuery("select SUM(total) as sumTotal from InvoiceDetail");
		)
		{
			rs.next();
			profit = rs.getDouble("sumTotal");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return profit;
	}
	
}
