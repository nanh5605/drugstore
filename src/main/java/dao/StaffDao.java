package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import common.ConnectDB;
import entity.Staff;

public class StaffDao {
	public List<Staff> getListStaff() {
		List<Staff> list = new ArrayList<Staff>();

		try(
			Connection con = ConnectDB.getConnect();
			CallableStatement cs = con.prepareCall("{call selStaff}");
			ResultSet rs = cs.executeQuery();			
			) {
			while(rs.next()) {
				Staff sta = new Staff();
				sta.setStaffId(rs.getInt("staffId"));
				sta.setFullname(rs.getNString("fullname"));
				sta.setDob(rs.getDate("dob").toLocalDate());
				sta.setGender(rs.getBoolean("gender"));
				sta.setIdentifierCode(rs.getInt("identifierCode"));
				sta.setPhone(rs.getNString("phone"));
				sta.setAddress(rs.getNString("address"));
				sta.setAccId(rs.getInt("AccId"));
				sta.setPicture(rs.getNString("picture"));
				sta.setStatus(rs.getBoolean("status"));
				list.add(sta);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return list;
	}
	
	public List<Staff> getListStaff(int identifierCode) {
//		var listN = new ArrayList<Employee>();
		List<Staff> list = new ArrayList<Staff>();

		try(
			Connection con = ConnectDB.getConnect();
			CallableStatement cs = con.prepareCall("{call selStaffbyidcode("+identifierCode+")}");
			ResultSet rs = cs.executeQuery();			
			) {
			while(rs.next()) {
				Staff sta = new Staff();
				sta.setPhone(rs.getNString("phone"));
				sta.setAddress(rs.getNString("address"));
				sta.setPicture(rs.getNString("picture"));
				list.add(sta);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return list;
	}
	
	public int AddStaff(Staff sta) {
		int i = 0;
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call AddStaff(?,?,?,?,?,?,?,?,?) }");
		){
			cs.setString(1, sta.getFullname());
			cs.setDate(2, Date.valueOf(sta.getDob()));
			cs.setBoolean(3, sta.isGender());
			cs.setInt(4, sta.getIdentifierCode());
			cs.setString(5, sta.getPhone());
			cs.setString(6, sta.getAddress());
			cs.setInt(7, sta.getAccId());
			cs.setString(8, sta.getPicture());
			cs.setBoolean(9, sta.isStatus());
			i = cs.executeUpdate();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return i;
	}
	
	public void UpdateStaff(Staff sta) {
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call UpdateStaff(?,?,?,?,?,?,?,?,?) }");
		){
			cs.setInt(1, sta.getStaffId());
			cs.setString(2, sta.getFullname());
			cs.setDate(3, Date.valueOf(sta.getDob()));
			cs.setBoolean(4, sta.isGender());
			cs.setInt(5, sta.getIdentifierCode());
			cs.setString(6, sta.getPhone());
			cs.setString(7, sta.getAddress());
			cs.setString(8, sta.getPicture());
			cs.setBoolean(9, sta.isStatus());
			cs.executeUpdate();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	public void DelStaff(Staff sta) {
		try(
			Connection connect = ConnectDB.getConnect();
			CallableStatement cs = connect.prepareCall("{call DelStaff(?) }");
		){
			cs.setInt(1, sta.getStaffId());
			cs.executeUpdate();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	public int checkexistStaff (int idcode, int accid) {
		int count = 0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call checkexistStaff("+idcode+","+accid+")}");
				ResultSet rs = cs.executeQuery();
			){
				while(rs.next()) {
					count = rs.getInt(1);
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		return count;
	}
	
	public int selectStaffByAccid(int accId) {
		int staffId = 0;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call selStaffidByAccid("+ accId +")}");
				ResultSet rs = cs.executeQuery();
				)
			{
				while(rs.next()) {
					int id = rs.getInt("staffId");
					staffId = id;
				}
			} catch (Exception e) {
				e.printStackTrace();
//				System.out.println(e.getMessage());
			}
		return staffId;
	}
	
	public String selectPictureByStaffId(int staffId) {
		String avatar = null;
		try(
				Connection connect = ConnectDB.getConnect();
				CallableStatement cs = connect.prepareCall("{call selPicByStaffId("+ staffId +")}");
				ResultSet rs = cs.executeQuery();
				)
			{
				while(rs.next()) {
					String img = rs.getString("picture");
					avatar = img;
				}
			} catch (Exception e) {
				e.printStackTrace();
//				System.out.println(e.getMessage());
			}
		return avatar;
	}
	
	public int sumStaff() {
		int total = 0;
		try 
		(
			Connection connect = ConnectDB.getConnect();
			Statement s = connect.createStatement();
			ResultSet rs = s.executeQuery("select count(staffId) as totalStaff from Staff");
		)
		{
			rs.next();
			total = rs.getInt("totalStaff");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return total;
	}
}
