package common;

import java.sql.*;

public class ConnectDB {
	
	private static Connection connection = null;
	private static String url = "jdbc:sqlserver://";
	private static String serverName = "MSI";
	private static String portNumber = "1433";
	private static String databaseName = "JavaPharmacyProject";
	private static String username = "sa";
	private static String password = "12345";
	private static String encrypt = "encrypt=true; trustServerCertificate=true";
	
	
//	String connectionURL = "jdbc:sqlserver://TONY\\SQL2019:1433;" + "databaseName=C2104L; user=sa; password=11111111; encrypt=true; trustServerCertificate=true";
	
	public static String getConnectionString() {
		
		return url + serverName + ":" + portNumber + ";" + "databaseName=" + databaseName + ";" + "user=" + username + ";" + "password=" + password + ";" + encrypt ;
		
	}

	public static Connection getConnect() {
		
		try {
			connection = DriverManager.getConnection(getConnectionString());
		} catch (Exception e) {
			e.printStackTrace();
			connection = null;
		}
		
		return connection;
	}
}
