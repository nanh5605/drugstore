package component;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import javax.swing.JPanel;

import model.Model_Menu;
import gui.JframeDashboard;
import gui.JframeLoading;
import gui.JframeLogin;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import swing.ListMenu;
import javax.swing.SwingConstants;

import event.EventMenuSelected;

public class Menu extends javax.swing.JPanel {
	private JPanel panelMoving;
	private JLabel lblNewLabel;
	private ListMenu listMenu;
	
	private EventMenuSelected event;
	private JLabel lblLogo;

    public void addEventMenuSelected(EventMenuSelected event) {
        this.event = event;
        listMenu.addEventMenuSelected(event);
    }
	
    public Menu() {
//        initComponents();
        setOpaque(false);
        setLayout(null);
        
        panelMoving = new JPanel();
       
       
        panelMoving.setOpaque(false);
        panelMoving.setBounds(0, 0, 215, 86);
        add(panelMoving);
        panelMoving.setLayout(null);
        
        lblNewLabel = new JLabel("ACT Pharmacy");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setForeground(Color.WHITE);
        lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 18));
        lblNewLabel.setBounds(58, 0, 147, 86);
        panelMoving.add(lblNewLabel);
        
        lblLogo = new JLabel("");
        lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
        ImageIcon dabIcon = new ImageIcon(JframeLoading.class.getResource("/icon/Medical-pharmacy-logo-design-template-on-transparent-background-PNG.png"));
		Image dabImage = dabIcon.getImage();
		Image modifiedDabImage = dabImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
		dabIcon = new ImageIcon(modifiedDabImage);
		lblLogo.setIcon(dabIcon);
        lblLogo.setBounds(3, 10, 62, 66);
        panelMoving.add(lblLogo);
        
        listMenu = new ListMenu();
        if (JframeLogin.level == 1 || JframeLogin.level == 2) {
        	 listMenu.setBounds(0, 100, 215, 381);
        } else {
        	 listMenu.setBounds(0, 100, 215, 150);
        }
        listMenu.setOpaque(false);
        add(listMenu);
        init();
    }
    
    private void init() {
    	if (JframeLogin.level == 1) {
    		listMenu.addItem(new model.Model_Menu("1", "Dashboard", model.Model_Menu.MenuType.MENU));
       	 	listMenu.addItem(new model.Model_Menu("2", "Account", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("3", "User", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("4", "Medicines", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("5", "Providers", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("6", "Sales", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("7", "Invoice", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("8", "Category", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("9", "Logout", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("10", "Exit", model.Model_Menu.MenuType.MENU));
    	} else if ( JframeLogin.level == 2) {
    		listMenu.addItem(new model.Model_Menu("1", "Dashboard", model.Model_Menu.MenuType.MENU));
    		listMenu.addItem(new model.Model_Menu("2", "Medicines", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("3", "Providers", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("4", "Category", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("5", "Sales", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("6", "Invoice", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("7", "Logout", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("8", "Exit", model.Model_Menu.MenuType.MENU));
    	} else {
    		listMenu.addItem(new model.Model_Menu("1", "Invoice", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("2", "Sales", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("3", "Logout", model.Model_Menu.MenuType.MENU));
            listMenu.addItem(new model.Model_Menu("4", "Exit", model.Model_Menu.MenuType.MENU));
    	}
    }

    @Override
    protected void paintChildren(Graphics grphcs) {
        Graphics2D g2 = (Graphics2D) grphcs;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint g = new GradientPaint(0, 0, Color.decode("#1CB5E0"), 0, getHeight(), Color.decode("#000046"));
        g2.setPaint(g);
        g2.fillRoundRect(0, 0, getWidth(), getHeight(), 15, 15);
        g2.fillRect(getWidth() -20, 0, getWidth(), getHeight());
        super.paintChildren(grphcs);
    }
}