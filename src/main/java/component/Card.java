package component;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SystemColor;

public class Card extends JPanel {
	private JLabel lblIcon;
	private JLabel lblTitle;
	private JLabel lblValue;
	private JLabel lblDes;

	/**
	 * Create the panel.
	 */
	public Color getColor1() {
        return color1;
    }

    public void setColor1(Color color1) {
        this.color1 = color1;
    }

    public Color getColor2() {
        return color2;
    }

    public void setColor2(Color color2) {
        this.color2 = color2;
    }

    private Color color1;
    private Color color2;
	public Card() {
		setBackground(Color.GRAY);
		setOpaque(false);
		color1 = Color.BLACK;
        color2 = Color.WHITE;
		setLayout(null);
	}
	
	 @Override
	    protected void paintComponent(Graphics grphcs) {
	        Graphics2D g2 = (Graphics2D) grphcs;
	        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        GradientPaint g = new GradientPaint(0, 0, color1, 0, getHeight(), color2);
	        g2.setPaint(g);
	        g2.fillRoundRect(0, 0, getWidth(), getHeight(), 15, 15);
	        g2.setColor(new Color(255, 255, 255, 50));
	        g2.fillOval(getWidth() - (getHeight() / 2), 10, getHeight(), getHeight());
	        g2.fillOval(getWidth() - (getHeight() / 2) - 20, getHeight() / 2 + 20, getHeight(), getHeight());
	        super.paintComponent(grphcs);
	    }
	 
}
