package helper;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Validation {
	public static boolean Checkempty(JTextField txtValue, String message) {
		if(txtValue.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(txtValue, message);
			return false;
		}
		return true;
	}
	
	public static boolean CheckRegex(JTextField txtValue,String regex,String message) {
		if(!txtValue.getText().matches(regex)) {
			JOptionPane.showMessageDialog(null, message);
			return false;
		}
		return true;
	}

	public static boolean Checkempty(JTextArea textArea, String message) {
		if(textArea.getText().trim().isEmpty()) {
			JOptionPane.showMessageDialog(textArea, message);
			return false;
		}
		return true;
	}
}
