package helper;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public final class RegexConst {
	public final static String NUMBER = "[0-9]+";
	
	public final static String PASSWORD = "[0-9]{6}";
	
	public final static String USERID = "[0-9]{8}";
	
	public final static String PHONE = "(84|0[3|5|7|8|9])+([0-9]{8})\\b";
	
	public final static String DATE = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";

	public static final String USER= "^([a-zA-Z]\\S*){2,25}$";
	
	public static final String CHARS = "^([a-zA-Z]\\s*){2,25}$";

	public static final String STATUS = "[0-1]";
	
	public static final String DOUBLE = "[+-]?([0-9]*[.])?[0-9]+";
	
	public static final String EMAIL = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@" 
	        + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

}

