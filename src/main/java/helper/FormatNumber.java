package helper;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class FormatNumber {
	public static String DoubleToString(double value) {
		return String.format("%,.2f", value);
	}
	public static String Replace(String value) {
		return value.replace(",","");
	}
}
