package entity;

import java.time.LocalDate;

public class Invoice {
	
	private String invoiceId;
	private LocalDate DateOfCreate;
	private int staffId;
	private double total;
	public Invoice(String invoiceId, LocalDate dateOfCreate, int staffId, double total) {
		this.invoiceId = invoiceId;
		DateOfCreate = dateOfCreate;
		this.staffId = staffId;
		this.total = total;
	}
	public Invoice() {
		
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public LocalDate getDateOfCreate() {
		return DateOfCreate;
	}
	public void setDateOfCreate(LocalDate dateOfCreate) {
		DateOfCreate = dateOfCreate;
	}
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "Invoice [invoiceId=" + invoiceId + ", DateOfCreate=" + DateOfCreate + ", staffId=" + staffId
				+ ", total=" + total + "]";
	}
	
	
}
