package entity;

import java.time.LocalDate;

public class Staff {
	private int staffId;
	private String fullname; 
	private LocalDate dob; 
	private boolean gender; 
	private int identifierCode;
	private String phone; 
	private String address; 
	private int AccId; 
	private String picture; 
	private boolean status;
	
	public Staff() {}

	public Staff(int staffId, String fullname, LocalDate dob, boolean gender, int identifierCode, String phone,
			String address, int accId, String picture, boolean status) {
		this.staffId = staffId;
		this.fullname = fullname;
		this.dob = dob;
		this.gender = gender;
		this.identifierCode = identifierCode;
		this.phone = phone;
		this.address = address;
		AccId = accId;
		this.picture = picture;
		this.status = status;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public int getIdentifierCode() {
		return identifierCode;
	}

	public void setIdentifierCode(int identifierCode) {
		this.identifierCode = identifierCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAccId() {
		return AccId;
	}

	public void setAccId(int accId) {
		AccId = accId;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Staff [staffId=" + staffId + ", fullname=" + fullname + ", dob=" + dob + ", gender=" + gender
				+ ", identifierCode=" + identifierCode + ", phone=" + phone + ", address=" + address + ", AccId="
				+ AccId + ", picture=" + picture + ", status=" + status + "]";
	}
	
	
	
	
}
