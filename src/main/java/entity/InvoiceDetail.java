package entity;

public class InvoiceDetail {
	private int invoiceDetailId, proId;
	private String invoiceId;
	private String proName;
	private String unit;
	private double price;
	private int quantity;
	private double total;
	
	public InvoiceDetail(int invoiceDetailId, int proId, String invoiceId, String proName, String unit, double price,
			int quantity, double total) {
		this.invoiceDetailId = invoiceDetailId;
		this.proId = proId;
		this.invoiceId = invoiceId;
		this.proName = proName;
		this.unit = unit;
		this.price = price;
		this.quantity = quantity;
		this.total = total;
	}
	
	public InvoiceDetail() {
		
	}

	public int getInvoiceDetailId() {
		return invoiceDetailId;
	}

	public void setInvoiceDetailId(int invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}

	public int getProId() {
		return proId;
	}

	public void setProId(int proId) {
		this.proId = proId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "InvoiceDetail [invoiceDetailId=" + invoiceDetailId + ", proId=" + proId + ", invoiceId=" + invoiceId
				+ ", proName=" + proName + ", unit=" + unit + ", price=" + price + ", quantity=" + quantity + ", total="
				+ total + "]";
	}
	
}
