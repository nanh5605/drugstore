package entity;

public class Account {
	private int AccId;
	private String username;
	private String password;
	private int level;
	
	public Account() {}
	public Account(int accId, String username, String password, int level) {
		AccId = accId;
		this.username = username;
		this.password = password;
		this.level = level;
	}
	
	public int getAccId() {
		return AccId;
	}
	public void setAccId(int accId) {
		AccId = accId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	
	@Override
	public String toString() {
		return "Account [AccId=" + AccId + ","
				+ "username=" + username + ", password=" 
				+ password + ", level=" + level + "]";
	}
	
}
